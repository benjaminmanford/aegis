





OpenEEG is an open source community hardware and software resource for low-cost electroencephalography (EEG). The OpenEEG software and hardware are released under a variety of free licenses. The Modular OpenEEG design is the most popular design used by OpenEEG developers and is supported by a range of documentation and a software development forum.
Modular OpenEEG devices were used for final-year EEG project themes involving aspects of signal processing and computer interfacing. The project applications ranged from gaming to health and provided opportunities to explore research areas such as affective computing, pervasive health and Brain-Computer Interfacing (BCI). The EEG project theme proved popular with students on consecutive years and provided scope for a broad range of individual projects. That is my main motivation on intergrating this technology into the health sector 

                                                           BACKGROUND
Some examples of EEG activities and projects in undergraduate electronic engineering education are reported in the literature. As would be anticipated, laboratory activities involving physiological signals are more commonly associated with biomedical engineering programs, where students may expect to perform exercises involving the acquisition and filtering of a variety of physiological signals with proprietary hardware and software Electronic engineering undergraduates with opportunities to take biomedical engineering modules might expect similar opportunities to work with commercial equipment. Rhodes and Dune outline a bioelectric potentials module for electronic engineering majors incorporating ECG (electrocardiography) amplifier and wave deflection detector circuit construction activities, and the measurement of their own ECG, EEG and EMG (electromyography, muscle signal) signals using the ADInstruments PTB 32 Teaching System.

The taught components of undergraduate degree electronic engineering programs at the University of Birmingham do not use EEG equipment. However, relevant underlying theory and technological
  
components are taught in the form of analog and digital electronics, embedded systems and digital signal processing. Final-year projects involving physiological signal inputs would typically require significant effort on lower-level design and development, e.g., circuit design and construction, and achieving the basic signal interface. Modular OpenEEG open source hardware and software provided an interesting opportunity to resource final-year projects for the capture of EEG signals so that there could be more scope to focus on higher-level design and development. For example, students could invest more effort in implementing ideas relevant to research and the real-world, and more effort in signal processing, application design and experimental testing. In this way, the OpenEEG project themes provided an opportunity to bring together a selection of topics from the electronic engineering curriculum and place them in the context of current research and real-world applications. Additionally, OpenEEG provided an opportunity to achieve this at a modest cost.
In recent years, open source software has received increased interest for educational applications, primarily on financial grounds but also due to the possibility of customization  It has been noted, however, that the adoption of open source software is not entirely without cost [8]. The often sparse documentation and lack of customer support should be accounted for when considering open source solutions. Despite this, it is believed that the benefits of open source software for education generally outweigh the costs [8].
There is limited documentation regarding the use of OpenEEG in academic project work. Brainathlon is a Modular OpenEEG application developed as part of a master’s project. It is a game for one or two players that provides visualizations of selected frequencies which players can attempt to control. The Brainathlon application is one of the software resources available on the OpenEEG website for download. Another example of student work is BrainBay, it is also a software resource application on the OpenEEG website. It was designed as a brain-computer interface for people with limited physical function.

EEG signals. EEG signals can be acquired across the scalp. The International10-20 system, defines a set of 19 locations, shown in Fig. 1., for EEG electrode placement. The number of electrodes required, however, is much reduced by knowing the location of signals of interest.



                                                               reffrence 
Alfonsi, B., “Open source in the classroom”, IEEE Distributed Systems Online, vol. 6, no. 6, 2005.

A.Palke, “Brainathlon: Enhancing brainwave control through brain-controlled game play”, Master’s
thesis, Mills College, 2004, on-line at http://www.webkitchen.com/brainathlon/

C. Veigl, “BrainBay - An open source software for biosignal and camera-mouse applications”
International Conference on Computers Helping People with Special Needs (ICCHP) – Young
Researchers Consortium 2006, Linz, Austria, on-line at http://www.shifz.org/brainbay/

E. Niedermeyer, F. L. da Silva. Electroencephalography: Basic Principles, Clinical Applications,
and Related Fields. (5th ed.), Philadelphia, PA: Lippincott Williams & Wilkins, 2005, pp.139-141.

L. A. Farwel and E. Donchin, “Talking off the top of your head: toward a mental prosthesis
utilizing event-related brain potentials”, Electroencephalography and Clinical Neurophysiology, vol.
70, no. 6. pp. 510-523, Dec. 1988.

C. Guger, et al "How many people are able to control a P300-based-brain-computer interface
(BCI)?" Neuroscience Letters, vol. 462, no. 1, pp. 94-98., 2009.

D. J. Krusienski, E. W. Sellers, F. Cabestaing, S. Bayoudh, D. J. McFarland, T. M Vaughan and J.
R. Wolpaw, “A comparison of classification techniques for the P300 Speller”, Journal of Neural
Engineering, vol. 3, no. 4, pp. 299-305, 2006.

 M. Thulasidas, C. Guan, J. Wu, “Robust classification of EEG signal for brain-computer interface”,
IEEE Trans. Neural Syst. Rehabil. Eng.Trans. , vol. 14, no. 1, pp. 24-29, 2006.

J. I. Münßinger, S. Halder, S. C. Kleih, A. Furdea,V. Raco, A. Hösle, and A. Kübler, “Brain painting:
first evaluation of a new brain–computer interface application with ALS-patients and healthy
volunteers”, Frontiers in Neuroscience, 4, 182, pp1-11, Nov. 2010.
 
B. Rebsamen, C. Guan, H. Zhang, C. Wang, C. Teo, M. H. Ang, E. Burdet, “A brain controlled
wheelchair to navigate in familiar environments”, IEEE Trans. Neural Syst. Rehabil. Eng., vol.18,
no. 6, pp. 590–598, Dec. 2010.
[18] Dropbox Inc, https://www.dropbox.com
[19] Olimex EEG products on-line https://www.olimex.com/Products/EEG/
[20] MATLAB, The MathWorks Inc., 2013, www.mathworks.com
[21] B.Houston, “Exocortex,” 2012 [Online].
[22] Deiber, M.P. Ibanez, V. Honda, M. Sadato, N. Raman, R & Hallett, M. - “Cerebral processes
related to visuomotor Imagery and generation of simple finger movements studied with positron
emission tomography,” NeuroImage, vol. 7, pp. 73-85, 1998.

