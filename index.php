<?php 

	$pageTitle = "Home";
	include('include/head.php');

?>

	<body class="index">

		<!-- For mobile preview -->
		<script type="text/javascript">
			if ((window.location !== window.parent.location && !(/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()))) == true) { document.body.style.overflowY = "scroll"; }
		</script>
			
     	<?php include ('include/header.php');?>
	
	<div class="page-slider-wrap">
		<div id="page-slider" >
		<ul>
			<li data-transition="zoomout" data-masterspeed="1300">
				<img src="demo/img/wow.jpg"  style="width:100%;height:115%;" alt="bg"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">

				<!-- LAYER NR. 1 -->
				<div class="tp-caption slider-title text-left sfl" data-x="left" data-hoffset="-15" data-y="top" data-voffset="130" data-speed="1000" data-start="500" data-easing="Back.easeInOut" data-endspeed="300" style="color:#000; left: 40px; font-size: 41px; line-height: 148%; top: 34px; margin-left: 40px;">ENJOY YOUR PEACE OF MIND</div>

				<!-- LAYER NR. 2 -->
				<div class="tp-caption slider-sub-title text-left sfl" data-x="left" data-hoffset="-15" data-y="center" data-speed="1100" data-start="600" data-easing="Back.easeInOut" data-endspeed="300" style="color:#000; font-size: 23px; top: 120px; text-transform: inherit;margin-left: 40px; ">A comprehensive protection for your smartphone,<br/> tablets and laptops so you can keep <br/>up with your usual life</div>

				<!-- LAYER NR. 3 -->
				<a href="#" class="tp-caption sfb btn btn-big text-left" data-x="left" data-hoffset="-15" data-y="bottom" data-voffset="-130" data-speed="1000" data-start="700" data-easing="Back.easeInOut" data-endspeed="300" style="/*color:#52c7e5*/ background: #05a5c5; border-radius: 50px; top: 205px; margin-left: 30px;">REGISTER</a>
				</li>
			<li data-transition="zoomout" data-masterspeed="1300">

				<!-- MAIN IMAGE -->
			<img src="App/img/slider_2.jpg"  alt="bg"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
			</li>
			<li data-transition="zoomout" data-masterspeed="1300">

				<!-- MAIN IMAGE -->
			<img src="App/img/slider_3.jpg"  alt="bg"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">

				<!-- LAYER NR. 1 -->
				<div class="tp-caption slider-title sfr" data-x="15" data-y="top" data-voffset="130" data-speed="800" data-start="700" data-easing="Back.easeInOut" data-endspeed="300" style="color:#d4deed">Feel free with Jets</div>

				<!-- LAYER NR. 2 -->
				<div class="tp-caption slider-sub-title sfl" data-x="15" data-y="center" data-speed="1000" data-start="800" data-easing="Back.easeInOut" data-endspeed="300" style="color:#ecc5a8">Here you will find everything <br /> you need for you</div>

				<!-- LAYER NR. 3 -->
				<div class="tp-caption sfr btn btn-big" data-x="15" data-y="bottom" data-voffset="-130" data-speed="800" data-start="900" data-easing="Power3.easeInOut" data-endspeed="300" style="color:#1a3e83">Read More</div>

			</li>
		</ul>
		<div class="tp-bannertimer tp-bottom"></div>
	</div>
</div><!-- .page-slider-wrap -->

<div id="page-content" role="main">
	<div class="container">

		<!-- CONTENT -->
		<div id="content">
			<div class="container-out" style="padding-top: 14%;">

					<div class="title title-section">
						<h2>Enjoy your peace of mind</h2>
						<p>In facilisis eget nisi nec consectetur. Maecenas laoreet tellus varius, aliquet justo non, interdum metus.</p>
						<span class="sticker">
							<!--<i class="icon fa fa-cogs"></i>-->
						</span>
					</div><!-- .title.title-section -->

		<div class="row" style="padding-bottom: 10%; padding: 68px;">
			   <div class="col-sm-4" data-animate="fadeInUp">
                 <div class="iconbox iconbox-style3 iconbox-list">
				    <div class="iconbox-content" style="margin-left: inherit;">
						<div class="title">
							<center><h4>CONVENIENCE</h4></center>
						</div>
				        <img src="demo/img/elements/a.jpg"  alt="bg"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" style="padding-bottom: 18px;">
						<div class="text">
							<p style="font-size: 130%;font-weight: 100;">Est aenean platea libero, sagittis a faucibus est, nostra ipsum</p>
						</div>
				         <a href="#" class="btn btn-big" data-x="left" data-hoffset="-15" data-y="bottom" data-voffset="-130" data-speed="1000" data-start="700" data-easing="Back.easeInOut" data-endspeed="300" style="background: #00af84; border: 1px solid #00af84; padding: 5% ; width: -webkit-fill-available; width: -moz-available;" >See The Aegis Difference</a>
		            </div><!-- .iconbox-content -->
			      </div><!-- .iconbox -->
			    </div><!-- .col-sm-4 -->

			<div class="col-sm-4" data-animate="fadeInUp">
				<div class="iconbox iconbox-style3 iconbox-list">
					<div class="iconbox-content" style="margin-left: inherit;">
								<div class="title">
						<center><h4>PROTECTION</h4></center>
					            </div>
						<img src="demo/img/elements/b.jpg"  alt="bg"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" style="padding-bottom: 18px;">
						<div class="text">
						<p style="font-size: 130%;font-weight: 100;">Est aenean platea libero, sagittis a faucibus est, nostra ipsum</p>
						</div>
						<a href="#" class="btn btn-big" data-x="left" data-hoffset="-15" data-y="bottom" data-voffset="-130" data-speed="1000" data-start="700" data-easing="Back.easeInOut" data-endspeed="300" style=" background: #00af84; border: 1px solid #00af84; padding: 5% ; width: -webkit-fill-available; width: -moz-available; ">Discover Amazing Services</a>
			        </div><!-- .iconbox-content -->
			    </div><!-- .iconbox -->
			</div><!-- .col-sm-4 -->

			<div class="col-sm-4" data-animate="fadeInUp">
				<div class="iconbox iconbox-style3 iconbox-list">
				    <div class="iconbox-content" style="margin-left: inherit;">
						<div class="title">
					       <center><h4>PLAN</h4></center>
				        </div>
						<img src="demo/img/elements/c.jpg"  alt="bg"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" style="padding-bottom: 18px;">
						<div class="text">
						<p style="font-size: 130%;font-weight: 100;">We have you perfectly covered under our solid plans </p>
					    </div>
			            <a href="#" class="btn btn-big" data-x="left" data-hoffset="-15" data-y="bottom" data-voffset="-130" data-speed="1000" data-start="700" data-easing="Back.easeInOut" data-endspeed="300" style=" background: #00af84; border: 1px solid #00af84; padding: 5% ;width: -webkit-fill-available; width: -moz-available;">Explore Mobile Protection</a>
		            </div><!-- .iconbox-content -->
			    </div><!-- .iconbox -->
            </div><!-- .col-sm-4 -->
		</div><!-- .row -->


				<div class="container-out container-light container-no-bottom"  style="height:1010px;  background: -webkit-linear-gradient(bottom, rgba(255,255,255,0), rgba(31, 188, 219,1)); /* For Safari 5.1 to 6.0 */
				    background: -o-linear-gradient(bottom, rgba(255,255,255,0), rgba(31, 188, 219,1)); /* For Opera 11.1 to 12.0 */
				    background: -moz-linear-gradient(bottom, rgba(255,255,255,0), rgba(31, 188, 219,1)); /* For Firefox 3.6 to 15 */
				    background: linear-gradient(to bottom, rgba(255,255,255,0), rgba(31, 188, 219,1)); /* Standard syntax (must be last)*/">
					<!--<img src="../CORE/uploads/images/macbook-preview.png" alt="Macbook-preview">-->
						<div class="title title-section">
							<h2>Enjoy your peace of mind</h2>
							<p style="font-size: 130%;font-weight: 100;">In facilisis eget nisi nec consectetur. Maecenas laoreet tellus varius, aliquet justo non, interdum metus.</p>
						</div>
						<div id="grad">
							<div class="thumbnail" data-animate="fadeInUp">
							<div class="col-sm-6"  style="padding-top: 13%; border-right: 1px solid #eee; float: left;">
								<center><img src="demo/img/elements/Laptop.png" alt="Laptop-preview" style="max-width: 85%;"></center><br>
								<center><h2 style="color: white;">LAPTOP CARE</h2></center>
								<center><p style="color: white; font-size: 130%;font-weight: 100; width: 65%;">Don't face unexpected cost of repairs and replacements.</p>
								<a href="#login-register" class="btn" style="border-radius: 50px;background-color: #fff;border-color: #fff; color: #36c3df; padding: 3% 8%;">Learn more</a>
								</center>
							</div>
							<div class="col-sm-6" style="padding-top: 5%; float: right;">
								<center><img src="demo/img/elements/Phone.png" alt="Phone-preview" style="max-width: 77%;"></center><br>
								<center><h2 style="color: white;">MOBILE PHONE CARE</h2></center>
								<center><p style="color: white; font-size: 130%;font-weight: 100; width: 65%;">Don't buy smartphones twice. When stuff happens, We've got you covered.</p>
								<a href="#login-register" class="btn" style="border-radius: 50px;background-color: #fff;border-color: #fff; color: #36c3df; padding: 3% 8%;">Learn more</a>
								</center>
							</div>
							</div>
						</div>
				 </div>
			   <!-- </div>.container-out -->
			
			<!-- two much -->
			<div class="row" style="padding-bottom: 8%;">
					<div class="thumbnail" data-animate="fadeInUp">
					<div class="col-sm-6" style="padding-top: 5%;  border-right: 1px solid #eee;">
						<center><img src="demo/img/elements/Tablet.png" alt="Tablet-preview" style="max-width: 60%;"></center><br>
						<center><h2 >TABLET CARE</h2></center>
						<center><p style="font-size: 130%;font-weight: 100; width: 65%;">Save more than the cost of replacement when the unforesceen happens.</p>
						<a href="#login-register"  class="btn" style="border-radius: 50px;background-color: #00af84;border-color: #00af84; padding: 3% 8%;">Learn more</a>
						</center>
					</div>
					<div class="col-sm-6" style="padding-top: 5%; ">
						<center><img src="demo/img/elements/Camera.png" alt="Camera-preview" style="max-width: 63%;"></center><br>
						<center><h2>CAMERA CARE</h2></center>
					    <center><p style="font-size: 130%;font-weight: 100; width: 65%;">In facilisis eget Maecenas laoreet tellus varius, aliquet justo non, interdum metus.</p>
					    <a href="#login-register"  class="btn" style="border-radius: 50px;background-color: #00af84;border-color: #00af84; padding: 3% 8%;">Learn more</a>
					    </center>
					</div>
				</div>
				</div>
				<!-- end of two-->
			
			<div class="container-out container-image" style="background-image:url(demo/img/elements/replacement.jpg)" >

				<div class="aegisghana" style="padding: 5% 2%;">
					<h1 style="font-size: -webkit-xxx-large;">Get a replacement within 72 hours</h1>
					<h3 style="font-size: 17px; width: 65%;">In facilisis eget Maecenas laoreet tellus varius, aliquet justo non, interdum metus.</h3>
					<a href="#login-register" class="btn" style="background-color: transparent;border-width: 2px;border-color: white; padding: 1% 4%; font-size: large;">See how it works</a>
				</div>
			

			</div>

			</div><!-- .container-out -->
			
			<div class="container-out">

				<div class="title title-section" style="padding-top: 8%; margin-bottom: 1px;">
					<h2>The Aegis Difference</h2>
					<p>Integer vel lectus orci. Nam non purus at odio ultricies malesuada.</p>
					<br>
					<hr>
					
					
				</div><!-- .title.title-section -->

				<div data-animate="fadeInUp" style="padding: 70px; padding-top: 0px;">
					
	               <article class="post post-latest post-type-video" style="padding: 6% 4% 13% 4%;">
	               <div class="col-sm-4 panel hvr-grow-shadow  " style="padding: 7% 0% 7% 0%; border-radius: 7px;">
	               	<center><img src="demo/img/elements/icons/superior-technolgy.png" alt="technolgy" style="width: 25%;     padding-bottom: 6%;"></center>
	               	<center><a href="" style="color: #21a0ba; font-size: 150%; "> superior technology</a></center>
	               </div>
	               <div class="col-sm-4 panel hvr-grow-shadow  " style="padding: 7% 0% 7% 0%; border-radius: 7px;">
	               	<center><img src="demo/img/elements/icons/Unlimited-Repairs.png" alt="Repairs" style="width: 25%;     padding-bottom: 6%;"></center>
	               	<center><a href="" style="color: #21a0ba; font-size: 150%;"> Unlimited Repairs</a></center>
	               </div>
	               <div class="col-sm-4 panel hvr-grow-shadow  " style="padding: 7% 0% 7% 0%; border-radius: 7px;">
	               	<center><img src="demo/img/elements/icons/Personalized-service.png" alt="service" style="width: 25%;     padding-bottom: 6%;"></center>
	               	<center><a href="" style="color: #21a0ba; font-size: 150%;">Personalized service</a></center>
	               </div> <br>
	               <div class="col-sm-4 panel hvr-grow-shadow  " style="padding: 7% 0% 7% 0%; border-radius: 7px;">
	               	<center><img src="demo/img/elements/icons/Lost-device-tracking.png" alt="tracking" style="width: 25%;     padding-bottom: 6%;"></center>
	               	<center><a href="" style="color: #21a0ba; font-size: 150%;">Lost device tracking</a></center>
	               </div>
	               <div class="col-sm-4 panel hvr-grow-shadow  " style="padding: 7% 0% 7% 0%; border-radius: 7px;">
	               	<center><img src="demo/img/elements/icons/24-7-claim-filing.png" alt="claim" style="width: 25%;padding-bottom: 6%;"></center>
	               	<center><a href="" style="color: #21a0ba; font-size: 150%;">24/7 claim filing</a></center>
	               </div>
	               <div class="col-sm-4 panel hvr-grow-shadow  " style="padding: 7% 0% 7% 0%; border-radius: 7px;">
	               	<center><img src="demo/img/elements/icons/18-months-upgrade.png" alt="upgrade" style="width: 25%;     padding-bottom: 6%;"></center>
	               	<center><a href="" style="color: #21a0ba; font-size: 150%;">18 months phone upgrade</a></center>
	               </div>
	               
					
			       </article><!-- .post -->
		
				</div>

			</div><!-- .container-out -->





 <div class="all" style="width: 43%;float: left;">
				<img src="demo/img/elements/aegis_app_android.png" alt="aegis_app_android" style="float: left;margin-top: -143px;margin-left: -13%;max-width: 91%;max-height: 80%;">
				</div>
			 <div class="container-out container-light" data-animate="" style="background-color:#000;padding-top: 0px;padding-bottom: 0px;m;margin-bottom: 83px;padding-left: 4859px;padding-right: 4859px;height: 540px;">
               <div class="row">
               	<div class="col-sm-6" style="width: 43%;">

				</div>

				<div class="col-sm-6" >
					<div class="title title-section" style="padding-right: 43px; padding-top: 4%;">
					<h2 style="color:#fff ;font-size: 280%;text-align: right;line-height: 64px;font-weight: 100;">DOWNLOAD THE AEGIS MOBILE APP FOR MORE BENEFITS</h2>
					<p style="color:#fff ;text-align: right;">Integer vel lectus orci. Nam non purus at odio ultricies malesuada.</p>
					<br/>
					<br/>
					<a >
					<img src="App/img/icons/apple_app_store.png" alt="apple_app_store" style="margin-left: 30%; max-width: 33%;">
				      <img src="App/img/icons/google_play.png" alt="google_play" style="float: right; max-width: 33%;"> 
					</a>
					
					
					
					
					

				   </div><!-- .title.title-section -->
				</div>
               </div>
				

			</div><!-- .container-out -->


			<div class="container-out container-light" style="background-color: white;">

				<div class="title title-section" style="margin-bottom: 10px;">
					<h2>Trusted by top tier corporate bodies</h2>
					<p>Ut pellentesque augue dui.</p>
					<br>
					<hr>
				</div><!-- .title.title-section -->

				<div data-animate="fadeInLeft">
					<div class="carousel-wrap">
						<ul class="carousel-nav">
							<li><a href="#" class="btn btn-icon-prev prev" style="display: inline-block;background-color: rgba(5, 124, 255, 0.32);border-color: rgba(5, 124 ,255, 0.32);"></a></li>
							<li><a href="#" class="btn btn-icon-next next" style="display: inline-block;background-color: rgba(5, 124, 255, 0.32);border-color: rgba(5, 124 ,255, 0.32);"></a></li>
						</ul><!-- .carousel-nav -->
						<div class="clients carousel" data-visible="4">
								<div class="client">
									<img src="demo/img/elements/barclays.png" alt="Client0" style="width: 73%;">
								</div>
									<div class="client">
									<img src="demo/img/elements/MyBet.png" alt="Client1">
								</div>
								<div class="client">
									<img src="demo/img/elements/Vodafone-logo.png" alt="Client2" style="width: 40%;">
								</div>
								<div class="client">
									<img src="demo/img/elements/unicode.png" alt="Client3" style="max-width: 90%;">
								</div>
								
						</div><!-- .carousel -->
					</div><!-- .carousel-wrap -->
				</div>

			</div><!-- .container-out -->
			<div class="container-out container-image" style="background-color:url(CORE/uploads/images/page/sections/testimonial.jpg)">
			


				

			</div><!-- .container-out -->
		</div><!-- #content -->

	</div><!-- .container -->
</div><!-- #page-content -->

	<?php include ('include/footer.php');?>

	