<!DOCTYPE html>
<html> <!--<![endif]-->

<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
	<!-- Basic -->
	<meta charset="utf-8">
	<title>Aegis | Standard</title>
	<?php include('include/head.php');?>
</head>
<script src = "App/js/main.js"></script>
   <script src = "App/js/ajax.js"></script>
  <script>
  function _(id){ return document.getElementById(id); }
  function submitForm(){
  _("mybtn").disabled = true;
  _("status").innerHTML = 'Please wait ...';
  var formdata = new FormData();
  formdata.append( "n", _("n").value );
  formdata.append( "e", _("e").value );
  formdata.append( "m", _("m").value );
  var ajax = new XMLHttpRequest();
  ajax.open( "POST", "function/example_parser.php" );
  ajax.onreadystatechange = function() {
    if(ajax.readyState == 4 && ajax.status == 200) {
      if(ajax.responseText == "success"){
        _("my_form").innerHTML = '<br/><center><img src="App/img/Thanks.png" alt="Stantard" style="max-width: 8%;"></center><br><h2  style="color:#454847; font-size: 20px; font-weight: 100; margin-top: 0px; text-align: center; "> Thanks for messaging us '+_("n").value+', <br> we will respond shortly.</h2><br/><br/><br/>';
      } else {
        _("status").innerHTML = ajax.responseText;
        _("mybtn").disabled = false;
      }
    }
  }
  ajax.send( formdata );
}
    </script>
<body class="page-services">

	
	
	<!-- For mobile preview -->
	<script type="text/javascript">
		if ((window.location !== window.parent.location && !(/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()))) == true) { document.body.style.overflowY = "scroll"; }
	</script>

			
			
     <?php include ('include/header.php');?>
	
			<aside id="page-header" class="page-header-image page-header-medium " style="background-image:url(App/img/standard/standard_banner.jpg);  width: -webkit-fill-available;">
				<div class="page-header-inner">
					<div class="page-header-content">
						<div class="container">
															<div class="row">
									<div class="col-md-8 col-md-offset-2">

										<div class="page-header-box hide-to-bottom">
																							<h1 style="font-weight: 100;">Single Device Plan</h1>
																																		<hr />
												<p class="lead">The quick fox jummps the lazy dog the quick brown fox jumps over the lazy dog </p>
																					</div>

									</div>
								</div>
													</div>
					</div>
				</div>
							<div class="breadcrumbs hide-to-bottom">
					<div class="container">
						<ul>
							<li><a href="Home" class="home"></a></li>
							<li><a href="#">Single</a></li>
							
													</ul>
					</div>
				</div><!-- .breadcrumbs -->
					</aside><!-- #page-header -->
		<div id="page-content" role="main">
             <br>
             <br>
             <br>
             <center><div class="title title-main">
							<h4 style="font-size: 300%; color: #2699b7; padding-top: 25px; font-weight: 100; text-transform: capitalize;">Single device plan</h4>
						</div>
						<div class="text">
						<p style="font-size: 130%;font-weight: 100; color: #737883; margin-bottom: 100px; inline-size: 770px; padding-bottom: 35px;">Maecenas a leo vel urna consequat ornare. Cras placerat libero quis blandit sagittis. Suspendisse sollicitudin augue erat, vel euismod tortor ultrices et.</p>
						</div>
						<center>
					<img src="App/img/standard/microsoft_surace_book.png" alt="Stantard" data-animate="fadeInUp" style="width: 19%;margin-right: 50px;">

					<img src="App/img/standard/htc.png" alt="htc" data-animate="fadeInUp" style="width: 4.9%;margin-right: 50px;">

					<img src="App/img/standard/ipad.png" alt="ipad"  data-animate="fadeInUp" style="width: 7.8%;margin-right: 50px;">
						</center>

						<center><img src="App/img/standard/platform.png" alt="platform" data-animate="fadeIn" style="margin-top: -83px; margin-bottom: 20px;"></center>

			</center>
				<br>
				<br>
				<div class="row row-inline" style="padding-top: 70px;padding-bottom: 70px; background: #e8f2f7;">

				 <center><div class="title title-main">
							<h4 style="font-size: 300%; color: #2699b7; padding-top: 25px; font-weight: 100; text-transform: none;">Why protect your device?</h4>
						</div>
						<br/>
						<div class="text" style="padding: 0px 100px 0px 100px;">
						<p style="font-size: 125%;font-weight: 100; color: #737883; inline-size: 800px; margin-bottom: 50px; padding-bottom: 35px; line-height: 26px;">As mobile phone are getting bigger and more powerful, they're also becoming more expensive. If you bought your phone on contract then you might not realise its true replacement cost insurance is becoming increasingly popular, as they now cost as much as 5.200! Here are a few reasons why device care could save you a lot of money</p>
						</div>
						<center>
					<div class="row" style="padding: 0px 100px 0px 100px;">					
							
							
								<div class="col-sm-4 ">
								<img src="App/img/standard/loss-theft.png" alt="Frozen Berries" style="max-width: 25%;">
								<h4 style="text-align: center; color: #d0423c;">Theft or loss</h4>
								<p style="font-size: 125%;font-weight: 100; color: #737883; margin-bottom: 100px; margin-top: -22px; text-align: center;">As mobile phone are getting bigger and powerful, they'er also becoming more expensive. if you bought your phone on contract then.</p>
								</div>

								
								<div class="col-sm-4">
								<img src="App/img/standard/accidents-happen.png" alt="Frozen Berries" style="max-width: 25%;">
								    <h4 style="text-align: center; color: #d0423c;">Accidents happen</h4>
								    <p style="font-size: 125%;font-weight: 100; color: #737883; margin-bottom: 100px; margin-top: -22px; text-align: center;">As mobile phone are getting bigger and powerful, they'er also becoming more expensive. if you bought your phone on contract then. </p>
									
								</div>

								
								<div class="col-sm-4" >
								<img src="App/img/standard/short-manufacturer-warrnties.png" alt="Frozen Berries" style="max-width: 25%;">
									<h4 style="text-align: center; color: #d0423c;">Short device warranties</h4>
								    <p style="font-size: 125%;font-weight: 100; color: #737883; margin-bottom: 100px; margin-top: -22px; text-align: center;">As mobile phone are getting bigger and powerful, they'er also becoming more expensive. if you bought your phone on contract then.</p>
								</div>
					 </div>


				</div><!-- .row -->
				<br>
				<br>
				<br>
				<div class="row row-inline" style="padding: 10px 100px 75px 100px;">
				<div class="title title-section" style="margin-top: 10px;" >
							<h2 style="font-size: 300%; font-weight: 100; color: #143256;">With us come more benefits</h2>
						</div><!-- .title.title-section -->
						<div class="controlP1">
				<div class="col-sm-4">
					<p><img src="App/img/standard/check.png" style="max-width: 9%;margin-right: 10px; font-size: 125%;font-weight: 100;">Loss</p>
				</div>
				<div class="col-sm-4">
					<p><img src="App/img/standard/check.png" style="max-width: 9%;margin-right: 10px;font-size: 125%;font-weight: 100; ">Accidental damage</p>
				</div>
				<div class="col-sm-4">
					<p><img src="App/img/standard/check.png" style="max-width: 9%;margin-right: 10px; font-size: 125%;font-weight: 100;">24/7 online claim filing</p>
				</div>
				<div class="col-sm-4">
					<p><img src="App/img/standard/check.png" style="max-width: 9%;margin-right: 10px; font-size: 125%;font-weight: 100;">Theft</p>
				</div>
				<div class="col-sm-4">
					<p><img src="App/img/standard/check.png" style="max-width: 9%;margin-right: 10px; font-size: 125%;font-weight: 100;">Malicious Damage</p>
				</div>
				<div class="col-sm-4">
					<p><img src="App/img/standard/check.png" style="max-width: 9%;margin-right: 10px; font-size: 125%;font-weight: 100;">72-hour Replacement</p>
				</div>
				<div class="col-sm-4">
					<p><img src="App/img/standard/check.png" style="max-width: 9%;margin-right: 10px; font-size: 125%;font-weight: 100;">Unlimited repairs and fixes</p>
				</div>
				<div class="col-sm-4">
					<p><img src="App/img/standard/check.png" style="max-width: 9%;margin-right: 10px; font-size: 125%;font-weight: 100;">Liquid Damage</p>
				</div>
				<div class="col-sm-4">
					<p><img src="App/img/standard/check.png" style="max-width: 9%;margin-right: 10px; font-size: 125%;font-weight: 100;">18-months phone upgrade</p>
				</div>
				</div><!--controlP1 end-->

				<center><a href="login?key:395975983408503895835938598" class="btn" style="margin-top: 70px; border-radius: 50px;background-color: #1e91de;border-color: #1e91de;color: #ffffff;padding: 1.3% 14%;font-weight: 100; font-size: 15px;">GET A STANDARD QUOTE</a></center>

					
				</div><!-- .row -->

				<div class="container-out container-image" style="background-image:url(App/img/standard/banner.jpg)" >
				<div class="aegisghana" style="padding: 5% 6% 5% 3%;">
					<h1 style="font-size: 300%; font-weight: 100; text-align: center; margin-bottom: 13px;">Want to protect more then 1 device?</h1>

					<h3 style="font-size: 17px;  text-align: center; font-weight: 100; margin-top: -10px;">Get 10% discount when you protect your device.</h3>
					<center><a href="multidevice?Aegis:395975983408503895835938598" class="btn" style=" text-transform: none; background-color: transparent; border-width: 2px;border-color: white; padding: 1% 4%; font-size: large; font-weight: 100;">Check out our student package</a></center>
				</div>
			

			</div>

			<div class="container-out container-light" style="background: white;">
				<div class="row">
				<form method="post" id="my_form" onsubmit="submitForm(); return false;">
					<div class="col-sm-12">
						<div class="title title-section" style="margin-bottom: -20px;">
							<h2 style="font-size: 300%; font-weight: 100; color: #143256;">Have questions for us?</h2>
							<p>please register your complaint using the contact details below.</p>
						</div><!-- .title.title-section -->
                    </div><!-- .col-sm-12 -->
					<div class="col-sm-12" style="padding: 45px;padding-left: 130px;padding-right: 130px;">
					 
					 	<div class="form-control" style="padding: 0px 340px 0px 340px; border-color: white;">
							<div class="placeholder">
								
								<input class="form-control" type="text" placeholder="Full Name" maxlength="100" name="name" id="n" required style=" background: #a9a9a924;" />
							</div>
							<br>
							<div class="placeholder">
								<input class="form-control" type="email" placeholder="Email" maxlength="100" name="email" id="e" required  style=" background: #a9a9a924;"/>
							</div>
							<br/>
							<br/>
							<div class="placeholder">
								<textarea class="form-control" type="text" placeholder="Message" rows="10" maxlength="5000" name="message" id="m" name="message" id="message" required style=" background: #a9a9a924;" ></textarea>
							</div>
							<button id="mybtn" type="submit" class="btn btn-big" style="background-color: #2bb4dc; padding: 16px 60px;border-color: #3bb3e0; color: #ffffff;">SEND</button>
							<span id ="status" style="float: right;margin-top: 15px;"></span>
						</div>
						<br/>
						<br/>
					 
					</div><!-- .col-sm-12 -->
					</form>
				</div><!-- .row -->
				

			
			</div><!-- .container-out -->
		

		</div><!-- #content -->
	</div><!-- .container -->
</div><!-- #page-content -->

	<?php include ('include/footer.php');?>

</html>