function restrict(elem){
    var tf = _(elem);
    var rx = new RegExp;
    if(elem == "email"){
        rx = /[' "]/gi;
    }else if(elem == "fname"){
        rx = /[^a-z- ]/gi;
    }else if(elem == "lname"){
        rx = /[^a-z- ]/gi;
    }
    tf.value = tf.value.replace(rx, "");
}
function emptyElement(x){
    _(x).innerHTML = "";
}
function signup(){
    var l = _("lname").value;
    var f = _("fname").value;
    var e = _("email").value;
    var p1 = _("pass1").value;
    var status = _("status");
    if(l == "" || f == "" || e == "" || p1 == "" ){
        status.innerHTML = "Fill out all of the form data";
    }else {
        _("signupbtn").style.display = "none";
        status.innerHTML = 'please wait ...';
        var ajax = ajaxObj("POST", "check.php");
         ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) == true) {
                if(ajax.responseText != "signup_success"){
                    status.innerHTML = ajax.responseText;
                    _("signupbtn").style.display = "block";
                } else {
                    //window.scrollTo(0,0);
                    window.location="account.php?msg="+e+"";
                    //_("signupform").innerHTML = "<div><p style='color: #fff'>OK "+u+", check the email address provided <u>("+e+")</u> in a moment to complete the sign up process and to activate your account.</p>";
                }
            }
        }
        ajax.send("l="+l+"&f="+f+"&e="+e+"&p="+p1);
    }
}
function openTerms(){
    _("terms").style.display = "block";
    emptyElement("status");
}
/* function addEvents(){
    _("elemID").addEventListener("click", func, false);
}
window.onload = addEvents; */