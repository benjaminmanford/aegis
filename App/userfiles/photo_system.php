<?php 
session_start();
if(!isset($_SESSION['AGXusername']))
{
echo'<script>window.location="../../login.php";</script>';
         //end();
         exit();
     }

include ('../../connection/AGconect.php');
$usernumber = $_SESSION['AGXusername'];
?>
<?php 
if (isset($_FILES["avatar"]["name"]) && $_FILES["avatar"]["tmp_name"] != ""){
	$fileName = $_FILES["avatar"]["name"];
    $fileTmpLoc = $_FILES["avatar"]["tmp_name"];
	$fileType = $_FILES["avatar"]["type"];
	$fileSize = $_FILES["avatar"]["size"];
	$fileErrorMsg = $_FILES["avatar"]["error"];
	$kaboom = explode(".", $fileName);
	$fileExt = end($kaboom);
	list($width, $height) = getimagesize($fileTmpLoc);
	if($width < 10 || $height < 10){
		echo'<script>window.location="../../message.php?msg=ERROR: That image has no dimensions";</script>';
		end();
        exit();	
	}
	$db_file_name = rand(100000000000,999999999999).".".$fileExt;
	if($fileSize > 3145728) {
		echo'<script>window.location="../../message.php?msg=ERROR: Your image file was larger than 3mb";</script>';
		end();	
	} else if (!preg_match("/\.(jpg|png)$/i", $fileName) ) {
		echo'<script>window.location="../../message.php?msg=ERROR: Your image file was not jpg or png type";</script>';
		end();
	} else if ($fileErrorMsg == 1) {
		echo'<script>window.location="../../message.php?msg=ERROR: An unknown error occurred";</script>';
		end();
	}
	$sql = "SELECT avatar FROM Client_options WHERE usernumber='$usernumber' LIMIT 1";
	$query = mysqli_query($cn, $sql);
	$row = mysqli_fetch_row($query);
	$avatar = $row[0];
	if($avatar != ""){
		$picurl = "../../user/$usernumber/$avatar"; 
	    if (file_exists($picurl)) { unlink($picurl); }
	}
	$moveResult = move_uploaded_file($fileTmpLoc, "../../user/$usernumber/$db_file_name");
	if ($moveResult != true) {
		echo'<script>window.location="../../message.php?msg=ERROR: File upload failed";</script>';
		end();
	}
	function img_resize($target, $newcopy, $w, $h, $ext) {
    list($w_orig, $h_orig) = getimagesize($target);
    $scale_ratio = $w_orig / $h_orig;
    if (($w / $h) > $scale_ratio) {
           $w = $h * $scale_ratio;
    } else {
           $h = $w / $scale_ratio;
    }
    $img = "";
    $ext = strtolower($ext);
     if($ext =="png"){ 
      $img = imagecreatefrompng($target);
    } else { 
      $img = imagecreatefromjpeg($target);
    }
    $tci = imagecreatetruecolor($w, $h);
    // imagecopyresampled(dst_img, src_img, dst_x, dst_y, src_x, src_y, dst_w, dst_h, src_w, src_h)
    imagecopyresampled($tci, $img, 0, 0, 0, 0, $w, $h, $w_orig, $h_orig);
    imagejpeg($tci, $newcopy, 84);
    }
	$target_file = "../../user/$usernumber/$db_file_name";
	$resized_file = "../../user/$usernumber/$db_file_name";
	$wmax = 200;
	$hmax = 300;
	img_resize($target_file, $resized_file, $wmax, $hmax, $fileExt);
	$sql = "UPDATE Client_options SET avatar ='$db_file_name' WHERE usernumber='$usernumber' LIMIT 1";
	$query = mysqli_query($cn, $sql);
	mysqli_close($cn);
	echo'<script>window.location="../dexoption.php";</script>';
	end();
}
?>