<header id="header" class="media" style="padding: 30px;">
    <div class="pull-left h-logo">
        <a href="index.php" class="hidden-xs">
           <img class="img-responsive" src="img/logos/LOGO-LIGHT-MEDIUM.png" style="width: 67%;">
        </a>
        <div class="menu-collapse" data-ma-action="sidebar-open" data-ma-target="main-menu">
            <div class="mc-wrap">
                <div class="mcw-line top palette-White bg"></div>
                <div class="mcw-line center palette-White bg"></div>
                <div class="mcw-line bottom palette-White bg"></div>
            </div>
        </div>
    </div>
    <ul class="pull-right h-menu">
        <li class="hm-search-trigger">
            <a href="#" data-ma-action="search-open">
                <i class="hm-icon zmdi zmdi-search"></i>
            </a>
        </li>
        <li class="dropdown hidden-xs hidden-md hidden-lg">
            <a data-toggle="dropdown" href="#"><i class="hm-icon zmdi zmdi-more-vert"></i></a>
            <ul class="dropdown-menu dm-icon pull-right">
                <li class="hidden-xs">
                    <a data-action="fullscreen" href="#"><i class="zmdi zmdi-fullscreen"></i> Toggle Fullscreen</a>
                </li>
                <li>
                    <a href="settings.php"><i class="zmdi zmdi-settings"></i> Settings</a>
                </li>
            </ul>
        </li>
        <li class="hm-alerts" data-user-alert="sua-messages" data-ma-action="sidebar-open" data-ma-target="user-alerts">
            <a href="#"><i class="hm-icon zmdi zmdi-notifications"></i></a>
        </li>
        <li class="dropdown hm-profile">
            <a data-toggle="dropdown" href="#">

                <?php 

                    if (isset($profile_pics)) {
                        echo '<img id="defaultsmall" src="<?php echo $profile_pics; ?>">';
                    } else {
                        echo '<img src="img/default_profile_pic.jpg" >';
                    }

                ?>

                <span id="uploaded_imagesmall"></span>
            </a>
            <ul class="dropdown-menu pull-right dm-icon">
                <li>
                    <a href="#"><i class="zmdi zmdi-account"></i> View Profile</a>
                </li>
                <li>
                    <a href="settings.php"><i class="zmdi zmdi-settings"></i> Settings</a>
                </li>
                <li>
                    <a href="logout.php">
                        <i class="zmdi zmdi-time-restore"></i> Logout
                    </a>
                </li>
            </ul>
        </li>
    </ul>
    <div class="media-body h-search">
        <form class="p-relative" name="searchform" id="searchform" onsubmit="searchform(); return false; ">
            <input id="search" type="text" onblur="search()" onkeyup="restrict('searchtext')" class="hs-input" placeholder="What do you need help with?" style="border-radius: 50px;">
            <i class="zmdi zmdi-search hs-reset" data-ma-action="search-clear"></i>
        </form>
    </div>
</header>