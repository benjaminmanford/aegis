<?php
include_once("../../connection/AGconect.php");

if(isset($_POST["type"]) && !empty($_POST["type"])){

 $make = $cn->real_escape_string(trim($_POST['type']));

    //Get all state data
    $query = $cn->query("SELECT * FROM Aegisdevice_Type WHERE type = '$make'  ORDER BY brand  ASC");
    
    //Count total number of rows
    $rowCount = $query->num_rows;
    
    //Display states list
    if($rowCount > 0){
        echo '<option value="">Select make</option>';
        while($row = $query->fetch_assoc()){ 
            echo '<option value="'.$row['brand'].'">'.$row['brand'].'</option>';
        }
    }else{
        echo '<option value="">Make not available</option>';
    }
}
 
if(isset($_POST["type_id"]) && !empty($_POST["type_id"])){
     $brand = $cn->real_escape_string(trim($_POST['type_id']));
     $make = $cn->real_escape_string(trim($_POST['make']));

    //Get all device data
    $query = $cn->query("SELECT * FROM Aegisdevice WHERE type = '$make' AND brand = '$brand' ORDER BY device ASC");
    
    //Count total number of rows
    $rowCount = $query->num_rows;
    
    //Display device list
    if($rowCount > 0){
        echo '<option value="">Select device</option>';
        while($row = $query->fetch_assoc()){ 
            echo '<option value="'.$row['device'].'">'.$row['device'].'</option>';
        }
    }else{
        echo '<option value="">device not available</option>';
    }
}

?>