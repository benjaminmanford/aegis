<!DOCTYPE html>
	<html> <!--<![endif]-->

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
	<!-- Basic -->
	<meta charset="utf-8">
	<title>Aegis - Home</title>
	<meta name="keywords" content="aegisghana" />
	<meta name="description" content="AEGIS - Home">
	<meta name="author" content="aegisghana.com">

	<!-- Favicons -->
	<link rel="shortcut icon" type='image/x-icon' href="img/favicon/favicon.ico">
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/favicon/fapple-touch-icon-144x144-precomposed.html">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/favicon/fapple-touch-icon-114x114-precomposed.html">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/favicon/fapple-touch-icon-72x72-precomposed.html">
	<link rel="apple-touch-icon-precomposed" href="img/favicon/fapple-touch-icon-precomposed.html">

	<!-- Mobile Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="css/library/bootstrap/bootstrap.min.css" />

	<!-- AWESOME and ICOMOON fonts -->
	<link rel="stylesheet" href="css/fonts/awesome/css/font-awesome.css">
	<link rel="stylesheet" href="css/fonts/icomoon/style.css">

	<!-- Open Sans fonts -->
	<link rel="stylesheet"  href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800">

	<!-- Theme CSS -->
	<!-- <link rel="stylesheet" href="css/theme.min.css"> -->
	<link rel="stylesheet" href="css/theme.css">
	<link rel="stylesheet" href="css/hover.css" media="all">
	<link rel="stylesheet" href="css/theme-elements.min.css">
	<!-- Slider revolution -->
	<link rel="stylesheet" href="js/library/slider-revolution/css/settings.css">
	<link rel="stylesheet" href="css/color/blue.css" id="layout-color">

	<!-- Theme Options -->
	<link rel="stylesheet" href="../CORE/css/options.css">

	<link rel="stylesheet" href="css/library/animate/animate.min.css">

	<!-- Your styles -->
	<link rel="stylesheet" href="css/styles.css">
</head>

<body class="index">

	
	
	<!-- For mobile preview -->
	<script type="text/javascript">
		if ((window.location !== window.parent.location && !(/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()))) == true) { document.body.style.overflowY = "scroll"; }
	</script>

	
			
	<header id="header">
					<aside class="topbar" style="background-color: #113352; margin-top: 0px;">
				<div class="container">

					<ul class="touch">
						<li>
							<img src="img/phone.png" alt="Phone" title="Phone">
							<p>+2332-4903-5899<br />+2332-0445-6345</p>
						</li>
						<li>
							<img src="img/mail.png" alt="Mail" title="Mail">
							<p><a href="mailto:info@aegisghana.com">support@aegisghana.com</a><br /><a href="mailto:info@aegisghana.com">info@aegisghana.com</a></p>
						</li>
						<li>
							<img src="img/map.png" alt="Map" title="Map">
							<p>124 Freetown Avenue,<br />East Legon, Accra</p>
						</li>
					</ul><!-- .touch -->


					<ul class="user-nav">
						<li><a href="instract/login" data-toggle="modal" title="Create an account" class="btn" style="border-radius: 50px; background-color: #007aff;padding: 7px 13px 7px 13px; margin-right: 7px;">PAY PREMIUM</a></li>
						<li><a href="#login-register" data-toggle="modal" title="Create an account" class="btn" style="border-radius: 50px;background-color: transparent;border-width: 2px;border-color: white; padding: 7px 13px 7px 13px; margin-right: 7px;">GET A QUOTE</a></li>
						<li><a href="#login-register" data-toggle="modal" title="Create an account" class="btn" style="border-radius: 50px;background-color: #e74c3c;border-color: #e74c3c; padding: 7px 13px 7px 13px;">FILE A CLAIM</a></li>
					</ul><!-- .user-nav -->

				</div><!-- .container -->
			</aside><!-- .topbar -->
				<div class="navbar megamenu-width">
			<div class="container">

									<aside id="main-search">
						<form action="#" method="GET">
							<a href="#" class="close"><i class="icomoon-close"></i></a>
							<div class="form-field" style="">
								<div class="placeholder" style="">
									<label for="query">what do you need help with?</label>
									<input class="form-control" type="text" name="query" id="query" style="background: #d6dee1; border-color: white;color: #000"/>
								</div>
							</div>
						</form>
					</aside><!-- #main-search -->
				
				<div class="navbar-inner">

					<a href="index.html" class="logo"  style="height: 46px;">
						<img src="instract/App/img/icons/logos/LOGO-DARK-SMALL.png" alt="Aegis_logo " style="">
					</a><!-- .logo -->

					<ul id="mobile-menu">
						<li><a href="#login-register" data-toggle="modal" title="Log in">Log In</a></li>
						<li><a href="#login-register" data-toggle="modal" title="Create an account">Register</a></li>
						<li>
							<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
								<i class="fa fa-reorder"></i>
							</button>
						</li>
												<li><a class="btn-search" href="#"><i class="fa fa-search"></i></a></li>
											</ul><!-- #mobile-menu -->

					<ul id="main-menu" class="collapse navbar-collapse nav slide">
											<li class='active'><a href="index.html">Home</a>
						</li>
					
						<li class="megamenu"><a href="#">OUR DIFFERENCE</a>
							
						</li>
						<li><a href="#">PLANS<i class="carret"></i></a>
							<ul class="dropdown">
								<li><a href="shortcodes-iconbox.html">STANDARD</a></li>
								<li><a href="shortcodes-teaser.html">STUDENT</a></li>
								<li><a href="shortcodes-teaser.html">MULTI-DEVICE</a></li>
								<li><a href="shortcodes-alerts-messages.html">CORPORATE</a></li>
								<li><a href="shortcodes-call2action.html">FAMILY</a></li>
								<li><a href="features-animation.html">INSTITUTION</a></li>
							</ul>
						</li>
						<li style="padding-bottom: 30px; padding-right: 10px;"><a href="#">PROTECTION<i class="carret"></i></a>
							<ul class="dropdown">
								<li><a href="shortcodes-iconbox.html">MOBILE</a></li>
								<li><a href="shortcodes-teaser.html">TABLET</a></li>
								<li><a href="shortcodes-alerts-messages.html">LAPTOP</a></li>
								
							</ul>
						</li>
						<li style="padding-bottom: 30px; "><a href="#" style="padding-right: 25px; ">ABOUT<i class="carret"></i>
						</a>
						<ul class="dropdown">
								<li><a href="shortcodes-iconbox.html">ABOUT US</a></li>
								<li><a href="shortcodes-teaser.html">CONTACT US</a></li>
								<li><a href="shortcodes-alerts-messages.html">OUR TEAM</a></li>
								<li><a href="shortcodes-call2action.html">FAQS</a></li>
							</ul>
						</li>
						<li style="padding-bottom: 30px; "><a href="#" style="border: 1px solid #2e343f; margin-right: 12px; ">LOG IN / REGISTER
							
						</a>
						</li>
						
						<li class="search-nav">
							<a href="#" class="btn-search"><i class="fa fa-search"></i></a>
						</li>
										</ul><!-- #main-menu -->

				</div><!-- .navbar-inner -->

			</div><!-- .container -->
		</div><!-- .navbar -->
	</header><!-- #header -->
	
		<div class="page-slider-wrap" style="height: 600px; max-height: 600px;">
	<div id="page-slider" style="max-height: 600px; height: 600px;">
		<ul style="display: block; overflow: hidden; width: 100%; height: 100px; max-height: 600px;">
			<li data-transition="zoomout" data-masterspeed="1300" style="visibility: inherit; opacity: 1; z-index: 20; width: 100%; height: 130%; overflow: hidden;">

				<!-- MAIN IMAGE -->
				<img src="img/wow.jpg"  style="width:100%;height:115%;" alt="bg"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">

				<!-- LAYER NR. 1 -->
				<div class="tp-caption slider-title text-left sfl" data-x="left" data-hoffset="-15" data-y="top" data-voffset="130" data-speed="1000" data-start="500" data-easing="Back.easeInOut" data-endspeed="300" style="color:#000; font-size: 41px; line-height: 148%; top: 34px;">ENJOY YOUR PEACE OF MIND</div>

				<!-- LAYER NR. 2 -->
				<div class="tp-caption slider-sub-title text-left sfl" data-x="left" data-hoffset="-15" data-y="center" data-speed="1100" data-start="600" data-easing="Back.easeInOut" data-endspeed="300" style="color:#000; font-size: 23px; top: 120px; text-transform: inherit;">A comprehensive protection for your smartphone,<br/> tablets and laptops so you can keep <br/>up with your usual life</div>

				<!-- LAYER NR. 3 -->
				<a href="#" class="tp-caption sfb btn btn-big text-left" data-x="left" data-hoffset="-15" data-y="bottom" data-voffset="-130" data-speed="1000" data-start="700" data-easing="Back.easeInOut" data-endspeed="300" style="/*color:#52c7e5*/ background: #05a5c5; border-radius: 50px; top: 205px;">REGISTER</a>
		</ul>
		<div class="tp-bannertimer tp-bottom"></div>
	</div>
</div><!-- .page-slider-wrap -->

<div id="page-content" role="main">
	<div class="container">

		<!-- CONTENT -->
		<div id="content">
			<div class="container-out" style="padding-top: 14%;">

					<div class="title title-section">
						<h2>Enjoy your peace of mind</h2>
						<p>In facilisis eget nisi nec consectetur. Maecenas laoreet tellus varius, aliquet justo non, interdum metus.</p>
						<span class="sticker">
							<!--<i class="icon fa fa-cogs"></i>-->
						</span>
					</div><!-- .title.title-section -->

		<div class="row" style="padding-bottom: 10%; padding: 68px;">
			   <div class="col-sm-4" data-animate="fadeInUp">
                 <div class="iconbox iconbox-style3 iconbox-list">
				    <div class="iconbox-content" style="margin-left: inherit;">
						<div class="title">
							<center><h4>CONVENIENCE</h4></center>
						</div>
				        <img src="img/elements/a.jpg"  alt="bg"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" style="padding-bottom: 18px;">
						<div class="text">
							<p style="font-size: 130%;font-weight: 100;">Est aenean platea libero, sagittis a faucibus est, nostra ipsum</p>
						</div>
				         <a href="#" class="btn btn-big" data-x="left" data-hoffset="-15" data-y="bottom" data-voffset="-130" data-speed="1000" data-start="700" data-easing="Back.easeInOut" data-endspeed="300" style="background: #00af84; border: 1px solid #00af84; padding: 5% ; width: -webkit-fill-available; width: -moz-available;" >See The Aegis Difference</a>
		            </div><!-- .iconbox-content -->
			      </div><!-- .iconbox -->
			    </div><!-- .col-sm-4 -->

			<div class="col-sm-4" data-animate="fadeInUp">
				<div class="iconbox iconbox-style3 iconbox-list">
					<div class="iconbox-content" style="margin-left: inherit;">
								<div class="title">
						<center><h4>PROTECTION</h4></center>
					            </div>
						<img src="img/elements/b.jpg"  alt="bg"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" style="padding-bottom: 18px;">
						<div class="text">
						<p style="font-size: 130%;font-weight: 100;">Est aenean platea libero, sagittis a faucibus est, nostra ipsum</p>
						</div>
						<a href="#" class="btn btn-big" data-x="left" data-hoffset="-15" data-y="bottom" data-voffset="-130" data-speed="1000" data-start="700" data-easing="Back.easeInOut" data-endspeed="300" style=" background: #00af84; border: 1px solid #00af84; padding: 5% ; width: -webkit-fill-available; width: -moz-available; ">Discover Amazing Services</a>
			        </div><!-- .iconbox-content -->
			    </div><!-- .iconbox -->
			</div><!-- .col-sm-4 -->

			<div class="col-sm-4" data-animate="fadeInUp">
				<div class="iconbox iconbox-style3 iconbox-list">
				    <div class="iconbox-content" style="margin-left: inherit;">
						<div class="title">
					       <center><h4>PLAN</h4></center>
				        </div>
						<img src="img/elements/c.jpg"  alt="bg"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" style="padding-bottom: 18px;">
						<div class="text">
						<p style="font-size: 130%;font-weight: 100;">We have you perfectly covered under our solid plans </p>
					    </div>
			            <a href="#" class="btn btn-big" data-x="left" data-hoffset="-15" data-y="bottom" data-voffset="-130" data-speed="1000" data-start="700" data-easing="Back.easeInOut" data-endspeed="300" style=" background: #00af84; border: 1px solid #00af84; padding: 5% ;width: -webkit-fill-available; width: -moz-available;">Explore Mobile Protection</a>
		            </div><!-- .iconbox-content -->
			    </div><!-- .iconbox -->
            </div><!-- .col-sm-4 -->
		</div><!-- .row -->


				<div class="container-out container-light container-no-bottom"  style="height:1010px;  background: -webkit-linear-gradient(bottom, rgba(255,255,255,0), rgba(31, 188, 219,1)); /* For Safari 5.1 to 6.0 */
				    background: -o-linear-gradient(bottom, rgba(255,255,255,0), rgba(31, 188, 219,1)); /* For Opera 11.1 to 12.0 */
				    background: -moz-linear-gradient(bottom, rgba(255,255,255,0), rgba(31, 188, 219,1)); /* For Firefox 3.6 to 15 */
				    background: linear-gradient(to bottom, rgba(255,255,255,0), rgba(31, 188, 219,1)); /* Standard syntax (must be last)*/">
					<!--<img src="../CORE/uploads/images/macbook-preview.png" alt="Macbook-preview">-->
						<div class="title title-section">
							<h2>Enjoy your peace of mind</h2>
							<p style="font-size: 130%;font-weight: 100;">In facilisis eget nisi nec consectetur. Maecenas laoreet tellus varius, aliquet justo non, interdum metus.</p>
						</div>
						<div id="grad">

							<div class="col-sm-6" style="padding-top: 13%; border-right: 1px solid #eee; float: left;">
								<center><img src="img/elements/Laptop.png" alt="Laptop-preview" style="max-width: 85%;"></center><br>
								<center><h2 style="color: white;">LAPTOP CARE</h2></center>
								<center><p style="color: white; font-size: 130%;font-weight: 100; width: 65%;">Don't face unexpected cost of repairs and replacements.</p>
								<a href="#login-register" class="btn" style="border-radius: 50px;background-color: #fff;border-color: #fff; color: #36c3df; padding: 3% 8%;">Learn more</a>
								</center>
							</div>
							<div class="col-sm-6" style="padding-top: 5%; float: right;">
								<center><img src="img/elements/Phone.png" alt="Phone-preview" style="max-width: 77%;"></center><br>
								<center><h2 style="color: white;">MOBILE PHONE CARE</h2></center>
								<center><p style="color: white; font-size: 130%;font-weight: 100; width: 65%;">Don't buy smartphones twice. When stuff happens, We've got you covered.</p>
								<a href="#login-register" class="btn" style="border-radius: 50px;background-color: #fff;border-color: #fff; color: #36c3df; padding: 3% 8%;">Learn more</a>
								</center>
							</div>
						</div>
				 </div>
			   <!-- </div>.container-out -->
			
			<!-- two much -->
			<div class="row" style="padding-bottom: 8%;">
					<div class="thumbnail" data-animate="fadeInUp">
					<div class="col-sm-6" style="padding-top: 5%;  border-right: 1px solid #eee;">
						<center><img src="img/elements/Tablet.png" alt="Tablet-preview" style="max-width: 60%;"></center><br>
						<center><h2 >TABLET CARE</h2></center>
						<center><p style="font-size: 130%;font-weight: 100; width: 65%;">Save more than the cost of replacement when the unforesceen happens.</p>
						<a href="#login-register"  class="btn" style="border-radius: 50px;background-color: #00af84;border-color: #00af84; padding: 3% 8%;">Learn more</a>
						</center>
					</div>
					<div class="col-sm-6" style="padding-top: 5%; ">
						<center><img src="img/elements/Camera.png" alt="Camera-preview" style="max-width: 63%;"></center><br>
						<center><h2>CAMERA CARE</h2></center>
					    <center><p style="font-size: 130%;font-weight: 100; width: 65%;">In facilisis eget Maecenas laoreet tellus varius, aliquet justo non, interdum metus.</p>
					    <a href="#login-register"  class="btn" style="border-radius: 50px;background-color: #00af84;border-color: #00af84; padding: 3% 8%;">Learn more</a>
					    </center>
					</div>
				</div>
				</div>
				<!-- end of two-->
			
			<div class="container-out container-image" style="background-image:url(img/elements/replacement.jpg)" >

				<div class="aegisghana" style="padding: 5% 2%;">
					<h1 style="font-size: -webkit-xxx-large;">Get a replacement within 72 hours</h1>
					<h3 style="font-size: 17px; width: 65%;">In facilisis eget Maecenas laoreet tellus varius, aliquet justo non, interdum metus.</h3>
					<a href="#login-register" class="btn" style="background-color: transparent;border-width: 2px;border-color: white; padding: 1% 4%; font-size: large;">See how it works</a>
				</div>
			

			</div>

			</div><!-- .container-out -->
			
			<div class="container-out">

				<div class="title title-section" style="padding-top: 8%;">
					<h2>The Aegis Difference</h2>
					<p>Integer vel lectus orci. Nam non purus at odio ultricies malesuada.</p>
					
					<hr>
				</div><!-- .title.title-section -->

				<div data-animate="fadeIn">
					
	               <article class="post post-latest post-type-video" style="padding: 6% 4% 13% 4%;">
	               <div class="col-sm-4 hvr-grow-shadow " style="padding: 7% 0% 7% 0%;">
	               	<center><img src="img/elements/icons/superior-technolgy.png" alt="technolgy" style="width: 25%;     padding-bottom: 6%;"></center>
	               	<center><a href="" style="color: #21a0ba; font-size: 150%; "> superior technology</a></center>
	               </div>
	               <div class="col-sm-4 hvr-grow-shadow " style="padding: 7% 0% 7% 0%;">
	               	<center><img src="img/elements/icons/Unlimited-Repairs.png" alt="Repairs" style="width: 25%;     padding-bottom: 6%;"></center>
	               	<center><a href="" style="color: #21a0ba; font-size: 150%;"> Unlimited Repairs</a></center>
	               </div>
	               <div class="col-sm-4 hvr-grow-shadow " style="padding: 7% 0% 7% 0%;">
	               	<center><img src="img/elements/icons/Personalized-service.png" alt="service" style="width: 25%;     padding-bottom: 6%;"></center>
	               	<center><a href="" style="color: #21a0ba; font-size: 150%;">Personalized service</a></center>
	               </div> <br>
	               <div class="col-sm-4 hvr-grow-shadow " style="padding: 7% 0% 7% 0%;">
	               	<center><img src="img/elements/icons/Lost-device-tracking.png" alt="tracking" style="width: 25%;     padding-bottom: 6%;"></center>
	               	<center><a href="" style="color: #21a0ba; font-size: 150%;">Lost device tracking</a></center>
	               </div>
	               <div class="col-sm-4 hvr-grow-shadow " style="padding: 7% 0% 7% 0%;">
	               	<center><img src="img/elements/icons/24-7-claim-filing.png" alt="claim" style="width: 25%;padding-bottom: 6%;"></center>
	               	<center><a href="" style="color: #21a0ba; font-size: 150%;">24/7 claim filing</a></center>
	               </div>
	               <div class="col-sm-4 hvr-grow-shadow " style="padding: 7% 0% 7% 0%;">
	               	<center><img src="img/elements/icons/18-months-upgrade.png" alt="upgrade" style="width: 25%;     padding-bottom: 6%;"></center>
	               	<center><a href="" style="color: #21a0ba; font-size: 150%;">18 months phone upgrade</a></center>
	               </div>
	               
					
			       </article><!-- .post -->
		
				</div>

			</div><!-- .container-out -->





 <div class="all" style="width: 43%;float: left;">
				<img src="img/elements/aegis_app_android.png" alt="aegis_app_android" style="float: left;margin-top: -143px;margin-left: -13%;max-width: 91%;max-height: 80%;">
				</div>
			 <div class="container-out container-light" data-animate="" style="background-color:#000;padding-top: 0px;padding-bottom: 0px;m;margin-bottom: 83px;padding-left: 4859px;padding-right: 4859px;height: 540px;">
               <div class="row">
               	<div class="col-sm-6" style="width: 43%;">

				</div>

				<div class="col-sm-6" >
					<div class="title title-section" style="padding-right: 43px; padding-top: 4%;">
					<h2 style="color:#fff ;font-size: 280%;text-align: right;line-height: 64px;font-weight: 100;">DOWNLOAD THE AEGIS MOBILE APP FOR MORE BENEFITS</h2>
					<p style="color:#fff ;text-align: right;">Integer vel lectus orci. Nam non purus at odio ultricies malesuada.</p>
					<br/>
					<a >
						<button style="background-color:#FFF;border-width: 0px;border-color: white;padding: 1% 8%; float: inline-end;font-size: small;color:#000;border-radius: 50px;">Appstore</button>
					<button style="background-color:#FFF; margin-right: 10px; border-width: 0px;border-color: white;padding: 1% 8%; float: inline-end;font-size: small;color:#000;border-radius: 50px;">Playstore</button>
					</a>
					
					
					
					
					

				   </div><!-- .title.title-section -->
				</div>
               </div>
				

			</div><!-- .container-out -->


			<div class="container-out container-light" style="background-color: white;">

				<div class="title title-section" style="margin-bottom: 10px;">
					<h2>Trusted by top tier corporate bodies</h2>
					<p>Ut pellentesque augue dui.</p>
					<br>
					<hr>
				</div><!-- .title.title-section -->

				<div data-animate="slideInRight">
					<div class="carousel-wrap">
						<ul class="carousel-nav">
							<li><a href="#" class="btn btn-icon-prev prev" style="display: inline-block;background-color: rgba(5, 124, 255, 0.32);border-color: rgba(5, 124 ,255, 0.32);"></a></li>
							<li><a href="#" class="btn btn-icon-next next" style="display: inline-block;background-color: rgba(5, 124, 255, 0.32);border-color: rgba(5, 124 ,255, 0.32);"></a></li>
						</ul><!-- .carousel-nav -->
						<div class="clients carousel" data-visible="4">
								<div class="client">
									<img src="img/elements/barclays.png" alt="Client0" style="width: 73%;">
								</div>
									<div class="client">
									<img src="img/elements/MyBet.png" alt="Client1">
								</div>
								<div class="client">
									<img src="img/elements/Vodafone-logo.png" alt="Client2" style="width: 40%;">
								</div>
								<div class="client">
									<img src="img/elements/unicode.png" alt="Client3" style="max-width: 90%;">
								</div>
								
						</div><!-- .carousel -->
					</div><!-- .carousel-wrap -->
				</div>

			</div><!-- .container-out -->
			<div class="container-out container-image" style="background-color:url(../CORE/uploads/images/page/sections/testimonial.jpg)">
			


				

			</div><!-- .container-out -->
		</div><!-- #content -->

	</div><!-- .container -->
</div><!-- #page-content -->

	<footer id="footer" style="background-color: #123354;">
		<div class="container">
			<div class="row row-inline">
				<div class="col-sm-6">

					<div class="title title-main">
						<h5>Newsletter</h5>
					</div>
					<div class="text">
						<p>Keep up on our always evolving product features and technology. Enter your e-mail and subscribe to our newsletter.</p>
					</div>

				</div><!-- .col-sm-6 -->
				<div class="col-sm-6">

					<form action="http://Aegis.funcoders.com/demo/php/newsletter.php" method="POST" class="form-validate" data-submit="ajax">
						<button type="submit" class="btn btn-big" style="float: inline-end;background-color: white; border-color: #123354; color: #123354;">SEND</button>
						<div class="form-field" style="width: 77%;">
							<div class="placeholder">
								<label for="subscribeemail">email...</label>
								<input class="form-control" type="email" maxlength="100" name="subscribeemail" id="subscribeemail" required />
							</div>
						</div>
					</form>

				</div><!-- .col-sm-6 -->
			</div><!-- .row-->

			<hr class="devider-heavy" />
			<div class="row">
				<div class="col-sm-4">

					<div class="widget">
						<div class="widget-heading">

							<div class="title title-main">
								<a href="index.html">
									<img src="instract/App/img/LOGO/WITH SLOGAN/LOGO-LIGHT-SMALL.png" alt="Aegisghana">
								</a>
							</div>

						</div><!-- .widget-heading -->
						<div class="widget-content">

							<div class="text">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur augue nunc, tempus id imperdiet eu.</p>
								<p>Mauris pulvinar est in quam dapibus a bibendum. Lorem ipsum dolor sit amet, consectetur Lorem ipsum dolor. </p>
							</div>

						</div><!-- .widget-content -->
					</div><!-- .widget -->

				</div><!-- .col-sm-3 -->
				<div class="col-sm-2">

					<div class="widget">
						<div class="widget-heading">

							<div class="title title-main">
								<h5>PLANS</h5>
							</div>

						</div><!-- .widget-heading -->
						<div class="widget-content">

							<section class="posts">
								<article class="post post-mini post-type-text">
								<ul class="nav">
								  <a href="index.html">STANDARD</a><br/>
								  <a href="#">STUDENT</a><br/>
								  <a href="#">CORPORATE</a><br/>
								  <a href="#">FAMILY</a><br/>
								  <a href="#">INSTITUTION</a><br/>
								  <a href="#">MULTI-DEVICE</a><br/>
								</ul>
									
								</article><!-- .post -->
							</section><!-- .posts -->

						</div><!-- .widget-content -->
					</div><!-- .widget -->

				</div><!-- .col-sm-2 -->




                <div class="col-sm-2">

					<div class="widget">
						<div class="widget-heading">

							<div class="title title-main">
								<h5>PROTECTION</h5>
							</div>

						</div><!-- .widget-heading -->
						<div class="widget-content">

							<section class="posts">
								<article class="post post-mini post-type-text">
								<ul class="nav">
								  <a href="index.html">MOBILE PHONE</a><br/>
								  <a href="#">TABLET</a><br/>
								  <a href="#">LAPTOP</a><br/>
								  <!--<a href="#">CAMERA</a><br/>-->
								</ul>
									
								</article><!-- .post -->
							</section><!-- .posts -->

						</div><!-- .widget-content -->
					</div><!-- .widget -->

				</div><!-- .col-sm-2 -->


				<div class="col-sm-2">

					<div class="widget">
						<div class="widget-heading">

							<div class="title title-main">
								<h5>ABOUT</h5>
							</div>

						</div><!-- .widget-heading -->
						<div class="widget-content">

							<section class="posts">
								<article class="post post-mini post-type-text">
								<ul class="nav">
								  <a href="index.html">OUR TEAM</a><br/>
								  <a href="#">CONTACT US</a><br/>
								  <a href="#">ABOUT US</a><br/>
								  <a href="#">OUR DIFFERENCE</a><br/>
								  <a href="#">FAQ</a><br/>
								  <!--<a href="#">BLOG</a><br/>-->
								</ul>
									
								</article><!-- .post -->
							</section><!-- .posts -->

						</div><!-- .widget-content -->
					</div><!-- .widget -->

				</div><!-- .col-sm-2 -->


				<div class="col-sm-2">

					<div class="widget">
						<div class="widget-heading">

							<div class="title title-main">
								<h5>QUICK LINKS</h5>
							</div>

						</div><!-- .widget-heading -->
						<div class="widget-content">

							<section class="posts">
								<article class="post post-mini post-type-text">
								<ul class="nav">
								  <a href="index.html">LOG IN</a><br/>
								  <a href="#">CREATE AN ACCOUNT</a><br/>
								  <a href="#">FILE A CLAIM</a><br/>
								  <a href="#">FAMILY</a><br/>
								  <a href="#">GET A QUOTE</a><br/>
								</ul>
									
								</article><!-- .post -->
							</section><!-- .posts -->

						</div><!-- .widget-content -->
					</div><!-- .widget -->

				</div><!-- .col-sm-2 -->

				
				
			</div><!-- .row-->
			<hr class="devider-heavy" />
			<ul class="nav text-center">
				<li><a href="index.html">Home</a></li>
				<li><a href="#">Features</a></li>
				<li><a href="#">Shortcodes</a></li>
				<li><a href="#">Portfolio</a></li>
				<li><a href="#">Pages</a></li>
				<li><a href="#">Blog</a></li>
				<li><a href="#">Contact</a></li>
			</ul>
			<hr class="devider-heavy" />
			<div class="widget">
						<div class="widget-heading">

							<div class="title title-main">
								<h5>WE ACCEPT THE FOLLOWING FORM OF PAYMENT</h5>
							</div>

						</div><!-- .widget-heading -->
			<ul class="nav">
				<a><img src="img/elements/visa.png" alt="visa" style="height: 15%; width: 11.95%;"></a>
				<a><img src="img/elements/mastercard.png" alt="mastercard" style="height: 15%; width: 12.06%;"></a>
				<a><img src="img/elements/tigo-cash.png" alt="tigo" style="height: 15%; width: 7.33%;"></a>
				<a><img src="img/elements/airtel-money.png" alt="airtel" style="height: 15%; width: 9.64%;"></a>
				<a><img src="img/elements/vodafone-cash.png" alt="vodafone" style="height: 15%; width: 12.75%;"></a>
				<a><img src="img/elements/mtn-mobile-money.png" alt="mtn" style="height: 15%; width: 8.56%;"></a>
			</ul>
			<hr class="devider-heavy" />
			<div class="row-inline-wrap">
				<div class="row row-inline">
					<div class="col-md-7">

						<ul class="touch">
							<li><i class="fa icomoon-location"></i><p>124 Freetown Avenue,<br />East Legon, Accra</p></li>
							<li><i class="fa fa-phone"></i><p>+2332-4903-5899<br />+2332-0445-6345</p></li>
							<li><i class="fa fa-envelope"></i><p><a href="mailto:supprot@aegisghana.com">supprot@aegisghana.com</a><br /><a href="mailto:info@aegisghana.com">info@aegisghana.com</a></p></li>
						</ul>

					</div><!-- .col-md-7 -->
					<div class="col-md-5">

						<ul class="social">
							<!--<li><a href="#" class="rss"></a></li>-->
							<li><a href="#" class="google"></a></li>
							<!--<li><a href="#" class="vimeo"></a></li>-->
							<li><a href="#" class="youtube"></a></li>
							<li><a href="#" class="facebook"></a></li>
							<li><a href="#" class="twitter"></a></li>
						</ul>

					</div><!-- .col-md-5 -->
				</div><!-- .row -->
			</div><!-- .row-inline-wrap -->
		</div><!-- .container -->
		<div class="credits" style="background-color: #112c47">Aegisghana © 2017<span>|</span><a href="#">Terms</a><span>|</span><a href="#">Legal Notice</a></div>
	</footer>


	<!-- Login/Register Modal -->
	<div id="login-register" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
		<div class="modal-dialog container">
			<div class="row">

				<div class="modal-bg" data-dismiss="modal"></div>

				<div class="col-sm-6">

					<div class="modal-body">
						<div class="modal-content">
							<a href="#" type="button" class="close btn btn-dark btn-icon-close" data-dismiss="modal" aria-hidden="true"></a>
							<div class="tab" data-animation="slide">
								<ul class="tab-heading">
									<li class="current"><h6><a href="#">Log in</a></h6></li>
									<li><h6><a href="#">Register</a></h6></li>
								</ul><!-- .tab-heading -->
								<div class="tab-content">
									<div class="current">

										<form action="http://jets.funcoders.com/demo/php/your-action.php" method="POST" class="form-validate" id="login">
											<div class="form-field">
												<div class="row">
													<label for="login-username" class="col-sm-3">Username<span class="require">*</span></label>
													<div class="col-sm-9">
														<input class="form-control" type="text" name="login-username" id="login-username" required />
													</div>
												</div>
											</div><!-- .form-field -->
											<div class="form-field">
												<div class="row">
													<label for="login-password" class="col-sm-3">Password<span class="require">*</span></label>
													<div class="col-sm-9">
														<input class="form-control" type="password" name="login-password" id="login-password" required />
													</div>
												</div>
											</div><!-- .form-field -->
											<div class="form-field">
												<div class="row">
													<div class="col-sm-offset-3 col-sm-9">
														<input type="checkbox" name="login-remember" id="login-remember">
														<label for="login-remember">Remember me</label>
													</div>
												</div>
											</div><!-- .form-field -->
											<div class="form-field text-right">
												<div class="form-group">
													Forgot your password? <a href="#" title="Password Lost and Found" class="recovery-open">Reset it here</a>
												</div>
												<div class="form-group">
													<input type="submit" value="Log in" class="btn">
												</div>
											</div><!-- .form-field -->
										</form>
										<form action="http://jets.funcoders.com/demo/php/your-action.php" method="POST" class="form-validate clear" id="recovery">
											<hr>
											<div class="title">
												<a href="#" class="recovery-close"><i class="fa fa-times"></i></a>
												<h4>Reset your password</h4>
											</div>
											<div class="text">
												<p>Enter your email address below and we'll send a special reset password link to your inbox.</p>
											</div>
											<div class="form-field">
												<div class="row">
													<label for="recovery-email" class="col-sm-3">Email<span class="require">*</span></label>
													<div class="col-sm-9">
														<input class="form-control" type="email" name="recovery-email" id="recovery-email" required />
													</div>
												</div>
											</div><!-- .form-field -->
											<div class="form-field text-right">
												<input type="submit" value="Send Recovery Email" class="btn">
											</div><!-- .form-field -->
										</form>
									</div>
									<div>
										<form action="http://jets.funcoders.com/demo/php/your-action.php" method="POST" class="form-validate" id="register">
											<div class="form-field">
												<div class="row">
													<label for="register-username" class="col-sm-3">Username<span class="require">*</span></label>
													<div class="col-sm-9">
														<input class="form-control" type="text" name="register-username" id="register-username" required />
													</div>
												</div>
											</div><!-- .form-field -->
											<div class="form-field">
												<div class="row">
													<label for="register-password" class="col-sm-3">Password <span class="require">*</span></label>
													<div class="col-sm-9">
														<input class="form-control" type="password" name="register-password" minlength="6" id="register-password" required />
														<p class="form-desc">6 or more characters</p>
													</div>
												</div>
											</div><!-- .form-field -->
											<div class="form-field">
												<div class="row">
													<label for="register-confirm" class="col-sm-3">Confirm <span class="require">*</span></label>
													<div class="col-sm-9">
														<input class="form-control" type="password" name="register-confirm" id="register-confirm" minlength="6" placeholder="Retype password" required />
													</div>
												</div>
											</div><!-- .form-field -->
											<div class="form-field">
												<div class="row">
													<label for="register-date" class="col-sm-3">Birth Date <span class="require">*</span></label>
													<div class="col-sm-9">
														<div class="form-group">
															<select name="register-month" id="register-date" required>
																<option value="">Month</option>
																<option value="January">January</option>
																<option value="February">February</option>
																<option value="Mars">Mars</option>
																<option value="April">April</option>
																<option value="May">May</option>
																<option value="June">June</option>
																<option value="July">July</option>
																<option value="September">September</option>
																<option value="October">October</option>
																<option value="November">November</option>
																<option value="December">December</option>
															</select>
														</div><!-- .form-group -->
														<div class="form-group-separator">/</div>
														<div class="form-group">
															<select name="register-day" required>
																<option value="">Day</option>
																<option value="1">1</option>
																<option value="2">2</option>
																<option value="3">3</option>
																<option value="4">4</option>
																<option value="5">5</option>
																<option value="6">6</option>
																<option value="7">7</option>
																<option value="8">8</option>
																<option value="9">9</option>
																<option value="10">10</option>
																<option value="11">11</option>
																<option value="12">12</option>
																<option value="13">13</option>
																<option value="14">14</option>
																<option value="15">15</option>
																<option value="16">16</option>
																<option value="17">17</option>
																<option value="18">18</option>
																<option value="19">19</option>
																<option value="20">20</option>
																<option value="21">21</option>
																<option value="22">22</option>
																<option value="23">23</option>
																<option value="24">24</option>
																<option value="25">25</option>
																<option value="26">26</option>
																<option value="27">27</option>
																<option value="28">28</option>
																<option value="29">29</option>
																<option value="30">30</option>
																<option value="31">31</option>
															</select>
														</div><!-- .form-group -->
														<div class="form-group-separator">/</div>
														<div class="form-group">
															<select name="register-year" required>
																<option value="">Year</option>
																<option value="2013">2013</option>
																<option value="2012">2012</option>
																<option value="2011">2011</option>
																<option value="2010">2010</option>
																<option value="2009">2009</option>
																<option value="2008">2008</option>
																<option value="2007">2007</option>
																<option value="2006">2006</option>
																<option value="2005">2005</option>
																<option value="2004">2004</option>
																<option value="2003">2003</option>
																<option value="2002">2002</option>
																<option value="2001">2001</option>
																<option value="2000">2000</option>
																<option value="1999">1999</option>
																<option value="1998">1998</option>
																<option value="1997">1997</option>
																<option value="1996">1996</option>
																<option value="1995">1995</option>
																<option value="1994">1994</option>
																<option value="1993">1993</option>
																<option value="1992">1992</option>
																<option value="1991">1991</option>
																<option value="1990">1990</option>
																<option value="1989">1989</option>
																<option value="1988">1988</option>
																<option value="1987">1987</option>
																<option value="1986">1986</option>
																<option value="1985">1985</option>
																<option value="1984">1984</option>
																<option value="1983">1983</option>
																<option value="1982">1982</option>
																<option value="1981">1981</option>
																<option value="1980">1980</option>
																<option value="1979">1979</option>
																<option value="1978">1978</option>
																<option value="1977">1977</option>
																<option value="1976">1976</option>
																<option value="1975">1975</option>
																<option value="1974">1974</option>
																<option value="1973">1973</option>
																<option value="1972">1972</option>
																<option value="1971">1971</option>
																<option value="1970">1970</option>
																<option value="1969">1969</option>
																<option value="1968">1968</option>
																<option value="1967">1967</option>
																<option value="1966">1966</option>
																<option value="1965">1965</option>
																<option value="1964">1964</option>
																<option value="1963">1963</option>
																<option value="1962">1962</option>
																<option value="1961">1961</option>
																<option value="1960">1960</option>
																<option value="1959">1959</option>
																<option value="1958">1958</option>
																<option value="1957">1957</option>
																<option value="1956">1956</option>
																<option value="1955">1955</option>
																<option value="1954">1954</option>
																<option value="1953">1953</option>
																<option value="1952">1952</option>
																<option value="1951">1951</option>
																<option value="1950">1950</option>
																<option value="1949">1949</option>
																<option value="1948">1948</option>
																<option value="1947">1947</option>
																<option value="1946">1946</option>
																<option value="1945">1945</option>
																<option value="1944">1944</option>
																<option value="1943">1943</option>
																<option value="1942">1942</option>
																<option value="1941">1941</option>
																<option value="1940">1940</option>
															</select>
														</div><!-- .form-group -->
													</div>
												</div>
											</div><!-- .form-field -->
											<div class="form-field">
												<div class="row">
													<div class="col-sm-offset-3 col-sm-9">
														<input type="checkbox" name="register-remember" id="register-remember">
														<label for="register-remember">Remember me</label>
													</div>
												</div>
											</div><!-- .form-field -->
											<div class="form-field text-right">
												<input type="submit" value="Register" class="btn">
											</div><!-- .form-field -->
										</form>
									</div>
								</div><!-- .tab-content -->
							</div><!-- .tab -->
						</div><!-- .modal-content -->
					</div><!-- .modal-body -->

				</div><!-- .col-sm-6 -->
			</div><!-- .row -->
		</div><!-- .modal-dialog -->
	</div><!-- #login-register-modal -->

	<a class="btn btn-icon-top" id="toTop" href="#"></a>

	<!-- jQuery & Helper library -->
	<script type="text/javascript" src="js/library/jquery/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="js/library/jquery/jquery-ui-1.10.4.custom.min.js"></script>

	<!-- Retina js -->
	<script type="text/javascript" src="js/library/retina/retina.min.js"></script>

	<!-- FancyBox -->
	<script type="text/javascript" src="js/library/fancybox/jquery.fancybox.pack8cbb.js?v=2.1.5"></script>

	<!-- Bootstrap js -->
	<script type="text/javascript" src="js/library/bootstrap/bootstrap.min.js"></script>

	<!-- Validate -->
	<script type="text/javascript" src="js/library/validate/jquery.validate.min.js"></script>

	<!-- FlickrFeed  -->
	<script type="text/javascript" src="js/library/jFlickrFeed/jflickrfeed.min.js"></script>
	
	<!-- carouFredSel -->
	<script type="text/javascript" src="js/library/carouFredSel/jquery.carouFredSel-6.2.1-packed.js"></script>
					
	<!-- Mediaelementjs -->
	<script type="text/javascript" src="js/library/mediaelementjs/mediaelement-and-player.min.js"></script>
		
	<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
	<script type="text/javascript" src="js/library/slider-revolution/js/jquery.themepunch.tools.min.js"></script>
	<script type="text/javascript" src="js/library/slider-revolution/js/jquery.themepunch.revolution.min.js"></script>
	<script type="text/javascript" src="js/slider.js"></script>
	
	<!-- Main theme javaScript file -->
	<script type="text/javascript" src="js/theme.js"></script>
	
	<!-- Options -->
	<script type="text/javascript" src="../CORE/js/library/jquery.cookie.js"></script>
	<script type="text/javascript" src="../CORE/js/options.js"></script>
	</body>

<!-- Mirrored from jets.funcoders.com/demo/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 17 Oct 2014 22:17:04 GMT -->
</html>