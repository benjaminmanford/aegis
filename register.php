<?php
require 'connection/AGconect.php';
session_start();

if(isset($_SESSION["AGXusername"])){
    echo'<script>window.location="App/dexoption.php";</script>';
         end();
    exit();
}

?>
<!DOCTYPE html>
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Register | Aegis</title>
        
            

         <!-- Vendor CSS -->
        <link href="App/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="App/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
        <!--<link href="App/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet">-->
        <link href="App/vendors/bower_components/google-material-color/dist/palette.css" rel="stylesheet">

        <link href="App/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
        <!--<link href="App/vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css" rel="stylesheet">-->
        <link href="App/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <!--<link href="App/vendors/farbtastic/farbtastic.css" rel="stylesheet">-->
        <!--<link href="App/vendors/bower_components/chosen/chosen.min.css" rel="stylesheet">-->
        <!--<link href="App/vendors/summernote/dist/summernote.css" rel="stylesheet">-->



        <!-- CSS -->
        <link href="App/css/app.min.1.css" rel="stylesheet">
        <link href="App/css/app.min.2.css" rel="stylesheet">

          <script src = "App/js/main.js"></script>
          <script src = "App/js/ajax.js"></script>
       
        <script>
            function restrict(elem){
                var tf = _(elem);
                var rx = new RegExp;
                if(elem == "email"){
                    rx = /[' "]/gi;
                } else if(elem == "number"){
                    rx = /[^0-9]/gi;
                }else if(elem == "fname"){
                    rx = /[^a-z-]/gi;
                }else if(elem == "lname"){
                    rx = /[^a-z-]/gi;
                }
                tf.value = tf.value.replace(rx, "");
            }

        function emptyElement(x){
            _(x).innerHTML = "";
        }

        function checkusername(){
            var u = _("email").value;
            if(u != ""){
                _("unamestatus").innerHTML = '<div class="call2action call2action-colored stripes animated"><div class="call2action-heading"><div class="text"><p> Validating Email... </p></div></div></div>';
                var ajax = ajaxObj("POST", "function/emailcheck.php");
                ajax.onreadystatechange = function() {
                    if(ajaxReturn(ajax) == true) {
                        _("unamestatus").innerHTML = ajax.responseText;
                    }
                }
                ajax.send("usernamecheck="+u);
            }
        }

         //function _(id){ return document.getElementById(id); }
         function signup(){

      // _("status").innerHTML = "<div class='preloader pls-bluegray'><svg class='pl-circular' viewBox='25 25 50 50'><circle class='plc-path' cx='50' cy='50' r='20'></circle></svg></div>";
            var l = _("lname").value;
            var f = _("fname").value;
            var n = _("number").value;
            var e = _("email").value;
            var p = _("pass1").value;
            var p1 = _("pass2").value;
            var dob = _("dob").value;
            var g =_("gender").value;
            var c = "web";
            var status = _("status").value;
            _("signupbtn").style.display = "none";
            _("status").innerHTML = 'please wait ...';
            if(l == "" || f == "" || n == "" || e == "" || p == "" || p1 == "" || dob == "" || g == "" || c == ""){
                    status.innerHTML = "Fill out all of the form data";
                } else if(p != p1){
                    status.innerHTML = "Your password fields do not match";
                } else if( _("terms").style.display == "none"){
                    status.innerHTML = "Please view the terms of use";
                } else {
                    _("signupbtn").style.display = "none";
                     _("status").innerHTML = 'please wait ...';
                    var ajax = ajaxObj("POST", "check.php");
                     ajax.onreadystatechange = function() {
                        if(ajaxReturn(ajax) == true) {
                            if(ajax.responseText != "signup_success"){
                                status.innerHTML = ajax.responseText;
                                _("signupbtn").style.display = "block";
                } else {
                            //window.scrollTo(0,0);
                            window.location="message.php?msg='Activation_sent'";
                            /*_("signupform").innerHTML ="<div class='bs-item z-depth-3-bottom'><br/>Hello "+f+" "+l+", kindly check the email address provided <u>("+e+")</u> in a moment to complete the sign up process and to activate your account.</div>";*/
                        }
                    }
                }
                ajax.send("l="+l+"&f="+f+"&n="+n+"&e="+e+"&p="+p+"&dob="+dob+"&g="+g+"&c="+c );
            }
        }

</script>
    </head>
    
    <body>
        <div class="login" data-lbg="teal" style="background-color: white;padding: 0px;width: 50%;float: left;">
           <!-- Login -->
           <div class="l-block toggled" id="l-login" style="max-width: 70%;">
               <form action="helloworld.php" method="POST">

                 <p><span id="unamestatus"></span></p>
                    <div class="lb-header palette-Teal bg" style="background-color: white;padding-bottom: 10px; padding-top: 0px;">
                       <center><img src="App/img/icons/logos/LOGO-DARK-SMALL.png" alt="Agises " style="padding-bottom: 15px;">
                                    <h3 style="font-size: 20px;">Create a free account!</h3>
                                    <small>Get up and run with Aegises in a few steps</small>
                       </center>
                    </div>

                    <!--<div class="lb-body" style="padding: 20px;">-->
                    <div class="form-group">
                        <div class="col-sm-6" style="padding: 0px;padding-right: 10px;">
                            <div class="form-group">
                                <div class="fg-line">
                                    <input type="text" id="fname" name="fname"  onkeyup="restrict('fname')" maxlength="30" class="form-control" placeholder="First name" required>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6" style="padding: 0px;padding-left: 10px;">
                            <div class="form-group">
                                <div class="fg-line">
                                    <input type="text" id="lname" name="lname"  onkeyup="restrict('lname')" maxlength="30" class="form-control"  placeholder="Other name" required>
                                </div>
                            </div>
                        </div>

                            <div class="col-sm-6" style="padding: 0px;padding-right: 10px;">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" id="email" name="email" onblur="checkusername()" onkeyup="restrict('email')" maxlength="88" class="form-control" placeholder="Email" required>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6" style="padding: 0px;padding-left: 10px;">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="text" id="number" name="number"  onkeyup="restrict('number')" maxlength="10" class="form-control" lass="form-control input-sm" data-trigger="hover" data-toggle="popover" data-placement="top" data-content="Note that the number you input in this filed must be active." title="" data-original-title="Input Note" placeholder="(+233) Phone Number" required>
                                    </div>
                                </div>
                            </div>
                            

                            <div class="col-sm-6" style="padding: 0px;padding-right: 10px;">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="password" id="pass1" name="pass1"  maxlength="32" class="input-sm form-control fg-input" placeholder="password" required>
                                        </div>
                                    </div>
                            </div>

                            <div class="col-sm-6" style="padding: 0px;padding-left: 10px;">
                                <div class="form-group">
                                    <div class="fg-line">
                                        <input type="password" id="pass2"  name="pass2" maxlength="32 " class="input-sm form-control fg-input" placeholder="Confirm Password" required>
                                        </div>
                                    </div>
                            </div>
                            
                         <div class="col-sm-6" style="padding: 0px;padding-right: 10px;">
                        <div class="form-group">
                                            <div class="dtp-container fg-line">
                                            <input type='text' id="dob" name="dob" class="form-control date-picker" placeholder="Enter date of birth ...">
                                        </div>
                                    </div>
                        </div>

                         <div class="col-sm-6" style="padding: 0px;padding-left: 10px;">
                         <div class="form-group">
                         <div class="fg-line">
                            <select class="selectpicker" id="gender" name="gender">
                                            <option><a>Gender</a></option>
                                            <option value="Male" >Male</option>
                                            <option value="Female">Female</option>
                            </select>
                            </div>
                            </div>
                        </div>
                        <!--<center><span id ="status"><div class='preloader pls-blue'><svg class='pl-circular' viewBox='25 25 50 50'><circle class='plc-path' cx='50' cy='50' r='20'></circle></svg></div></span></center>-->

                        <center>
                            <button type="submit" class="btn palette-Blue bg waves-effect" style="padding: 14px;padding-left: 133px;padding-right: 133px;border-radius: 50px; background-color: #267bbf;box-shadow: 0 0px 0px rgba(0, 0, 0, 0);">
                                REGISTER 
                            </button>
                        </center>
                       
                        <div class="m-t-20">
                            <center><a data-block="#l-forget-password" data-bg="purple" href="#"  style="color: #9b9b9b; font-size: 12px; ">By signing up you agree to our <strong>Terms of Service</strong> and <strong>Privacy Policy</strong> </a></center>

                            <center><h4 style="font-size: 16px">Already have an account? <a data-bg="blue"  href="login.php">log in</a></h4></center>
                        </div>
                    </div>
                </form>
           </div>
           
          
        </div>
        
        <div class="" data-lbg="teal" style="background-color: white;padding: 0px;width: 50%;float: right;">
           <!-- Login -->
          <img src="App/img/IMAGES/Registeration Page.jpg" alt="Agises " style="width: 100%; height: 100vh"> 
              <h3 style="    margin: -20px auto 0 auto; position: absolute; position: absolute;top: 23%;margin: 0 auto;  color: white;padding: 20px; padding-left: 10%;" >With Aegis, you're covered against...

              <h4 style=" margin: -20px auto 0 auto; position: absolute; position: absolute;top: 35%;margin: 0 auto;  color: white;padding: 20px; padding-left: 10%;">
                <p><i class="zmdi zmdi-check-circle zmdi-hc-fw" style="color: #0093af;"></i> <span>Theft</span></p>
                <p><i class="zmdi zmdi-check-circle zmdi-hc-fw" style="color: #0093af;"></i> <span>Damage</span></p>
                <p><i class="zmdi zmdi-check-circle zmdi-hc-fw" style="color: #0093af;"></i> <span>Immersion</span></p>
                <p><i class="zmdi zmdi-check-circle zmdi-hc-fw" style="color: #0093af;"></i> <span>Spill</span></p>
                <p><i class="zmdi zmdi-check-circle zmdi-hc-fw" style="color: #0093af;"></i> <span>Breakdown & More</span></p>
              </h4>
             </h3>
          </img>
                           
            
          
        </div>
        <!-- Older IE warning message -->
        <!--[if lt IE 9]>
            <div class="ie-warning">
                <h1 class="c-white">Warning!!</h1>
                <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
                <div class="iew-container">
                    <ul class="iew-download">
                        <li>
                            <a href="http://www.google.com/chrome/">
                                <img src="img/browsers/chrome.png" alt="">
                                <div>Chrome</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.mozilla.org/en-US/firefox/new/">
                                <img src="img/browsers/firefox.png" alt="">
                                <div>Firefox</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.opera.com">
                                <img src="img/browsers/opera.png" alt="">
                                <div>Opera</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.apple.com/safari/">
                                <img src="img/browsers/safari.png" alt="">
                                <div>Safari</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                                <img src="img/browsers/ie.png" alt="">
                                <div>IE (New)</div>
                            </a>
                        </li>
                    </ul>
                </div>
                <p>Sorry for the inconvenience!</p>
            </div>
        <![endif]-->
 


        <!-- Javascript Libraries -->
        <script src="App/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="App/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>


       <script src="App/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="App/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="App/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>

        <script src="App/vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="App/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
         <script src="App/vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js"></script>
         <script src="App/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

        <!--<script src="App/vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script>-->
        <script src="App/vendors/summernote/dist/summernote-updated.min.js"></script>

        <script src="App/vendors/bower_components/chosen/chosen.jquery.min.js"></script>
        <!--<script src="App/vendors/fileinput/fileinput.min.js"></script>-->
        <!--<script src="App/vendors/input-mask/input-mask.min.js"></script>-->
        <!--<script src="App/vendors/farbtastic/farbtastic.min.js"></script>-->

        <!-- Placeholder for IE9 -->
        <!--[if IE 9 ]>
            <script src="vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
        <![endif]-->

        <!--<script src="App/js/actions.js"></script>-->
        <script src="App/js/functions.js"></script>
        <!--<script src="App/js/demo.js"></script>-->
        
    </body>

</html>