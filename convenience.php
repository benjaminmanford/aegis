<!DOCTYPE html>
<html> <!--<![endif]-->

<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
	<!-- Basic -->
	<meta charset="utf-8">
	<title>Aegis | Convenience</title>
	<?php include('include/head.php');?>
</head>

<body class="page-services">

	
	
	<!-- For mobile preview -->
	<script type="text/javascript">
		if ((window.location !== window.parent.location && !(/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()))) == true) { document.body.style.overflowY = "scroll"; }
	</script>

			
			
     <?php include ('include/header.php');?>
	
			<aside id="page-header" class="page-header-image page-header-medium " style="background-image:url(App/img/banner.jpg)">
				<div class="page-header-inner">
					<div class="page-header-content">
						<div class="container">
															<div class="row">
									<div class="col-md-8 col-md-offset-2">

										<div class="page-header-box hide-to-bottom">
																							<h1>Our Difference</h1>
																																		<hr />
												<p class="lead">The quick fox jummps the lazy dog the quick brown fox jumps over the lazy dog </p>
																					</div>

									</div>
								</div>
													</div>
					</div>
				</div>
							<div class="breadcrumbs hide-to-bottom">
					<div class="container">
						<ul>
							<li><a href="index.html" class="home"></a></li>
							<li><a href="#">Our Difference</a></li>
							
													</ul>
					</div>
				</div><!-- .breadcrumbs -->
					</aside><!-- #page-header -->
		<div id="page-content" role="main">
             <br>
             <br>
             <br>
				<div class="row row-inline" style="padding: 75px;">
					<div class="col-sm-6">

						<div class="thumbnail">
							<img src="App/img/1.jpg" alt="Piano Keyboard">
						</div>

					</div><!-- .col-sm-6 -->
					<div class="col-sm-6" style="margin-top: -75px;">

						<div class="title title-main">
							<h4 style="font-size: 230%; color: #2699b7;">Save Money</h4>
						</div>

						<div class="text">
						<p style="font-size: 130%;font-weight: 100; color: #737883;">Maecenas a leo vel urna consequat ornare. Cras placerat libero quis blandit sagittis. Suspendisse sollicitudin augue erat, vel euismod tortor ultrices et.</p>
						</div>

					</div><!-- .col-sm-6 -->
				</div><!-- .row -->
				<br>
				<br>
				<div class="row row-inline" style="padding-top: 70px;padding-bottom: 70px; background: #0f2a44;">
					<div class="col-sm-6">

						<div class="title title-main text-right">
							<h4 style="font-size: 230%; color: #208fad;">72-Hour Replacement</h4>
						</div>

						<div class="text text-right">
							<p style="font-size: 130%;font-weight: 100; color: #ffffff;">Ut at auctor velit, accumsan eleifend velit. Morbi nibh tortor, laoreet quis enim ut, dapibus pellentesque nisl. Nam interdum laoreet felis, nec imperdiet neque mollis eu. In facilisis eget nisi nec consectetur. </p>
							
						</div>

					</div><!-- .col-sm-6 -->
					<div class="col-sm-6">

						<div class="thumbnail">
							<img src="" alt="Frozen Berries">
						</div>

					</div><!-- .col-sm-6 -->
				</div><!-- .row -->
				<br>
				<br>
				<br>
				<div class="row row-inline" style="padding: 75px;">

					<div class="col-sm-6">

						<div class="thumbnail">
							<img src="App/img/3.jpg" alt="Beautiful Winter Landscape">
						</div>

					</div><!-- .col-sm-6 -->
					<div class="col-sm-6">

						<div class="title title-main">
							<h4 style="font-size: 230%; color: #48a4c1;">Comprehensive Protection</h4>
						</div>

						<div class="text" style="font-size: 130%;font-weight: 100; color: #70727e;">
							<p>Integer viverra massa lobortis felis consectetur pretium. Aliquam sodales sem eros, sed interdum nulla vehicula in. Nulla facilisi. Vestibulum faucibus tortor eu auctor accumsan.</p>
							
						</div>

					</div><!-- .col-sm-6 -->
				</div><!-- .row -->
				<div class="container-out container-image" style="background-image:url(App/img/claims.jpg)" >

				<div class="aegisghana" style="padding: 5% 6% 5% 3%; width: 61%;">
					<h1 style="font-size: 300%; font-weight: 100; float: right; margin-bottom: 13px;">Easy Claims</h1>
					<br>
					<br>
					<h3 style="font-size: 17px;  text-align: right; font-weight: 100; padding-left: 80px;">In facilisis eget Maecenas laoreet tellus varius, aliquet justo non, interdum metus.</h3>
				</div>
			

			</div>

			<div class="container-out container-light">
				<div class="row">
					<div class="col-sm-12">

						<div class="title title-section">
							<h2 style="font-size: 300%; font-weight: 100; color: #143256;">Aegis Device care app provides</h2>
						</div><!-- .title.title-section -->
                    </div><!-- .col-sm-12 -->
					<div class="col-sm-12" style="padding: 80px;padding-left: 130px;padding-right: 130px;">
					<div class="col-sm-3">
					 <center><img src="App/img/2/A.png" alt="Tracking" style="max-width: 32%;"></center>
                    <h2 style="font-size: 120%;font-weight: 100; color: #56b0c9; margin-top: 10px; text-align: center;">Lost Device Tracking</h2>
					</div><!-- .col-sm-4 -->
					<div class="col-sm-3">
					<center><img src="App/img/2/B.png" alt="Device Locking" style="max-width: 32%;"></center>
                    <h2 style="font-size: 120%;font-weight: 100; color: #56b0c9; margin-top: 10px;text-align: center;">Remote Device Locking</h2>
					</div><!-- .col-sm-4 -->
					<div class="col-sm-3">
					<center><img src="App/img/2/C.png" alt="Claim Filing" style="max-width: 32%;"></center>
                    <h2 style="font-size: 120%;font-weight: 100; color: #56b0c9; margin-top: 10px;text-align: center;">Claim Filing & Tracking</h2>
					</div><!-- .col-sm-4 -->
					<div class="col-sm-3">
					<center><img src="App/img/2/D.png" alt="Account" style="max-width: 32%;"></center>
                    <h2 style="font-size: 120%;font-weight: 100; text-align: center; margin-top: 10px; color: #56b0c9;">Account Access</h2>
					</div><!-- .col-sm-4 -->
					</div><!-- .col-sm-12 -->
				</div><!-- .row -->
				

			
			</div><!-- .container-out -->
		

		</div><!-- #content -->
	</div><!-- .container -->
</div><!-- #page-content -->

	<?php include ('include/footer.php');?>

</html>