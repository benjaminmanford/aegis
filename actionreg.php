<?php
DEFINE('DS', DIRECTORY_SEPARATOR);
// Ajax calls this REGISTRATION code to execute
if(isset($_POST["n"])){
    //SMS API CONNECTION
    require "hubtel/Hubtel/Api.php";
    // CONNECT TO THE DATABASE
    include_once("connection/AGconect.php");
    // GATHER THE POSTED DATA INTO LOCAL VARIABLES
    $n = preg_replace('#[^0-9]#i', '', $_POST['n']);
    $f = preg_replace('#[^a-z]#i', '', $_POST['f']);
    $l = preg_replace('#[^a-z]#i', '', $_POST['l']);
    $e = mysqli_real_escape_string($cn, $_POST['e']);
    $dob = $cn->real_escape_string(trim($_POST['dob']));
    $p = $_POST['p'];
    $c = preg_replace('#[^a-z]#i', '', $_POST['c']);
    $s = preg_replace('#[^a-z]#i', '', $_POST['g']);


    	if ( $c == "web"){

    		if (filter_var($e, FILTER_VALIDATE_EMAIL)) {
    				//................web code to run.........................//
            // DUPLICATE DATA CHECKS FOR USERNAME AND EMAIL
            $sql = "SELECT id FROM Client WHERE email ='$e' LIMIT 1";
            $query = mysqli_query($cn, $sql); 
            $e_check = mysqli_num_rows($query);

                 // GET USER IP ADDRESS
		     if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		                $ip = $_SERVER['HTTP_CLIENT_IP'];
		            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		            } else {
		                $ip = $_SERVER['REMOTE_ADDR'];
		            }
		     // for locahost default ip ........
		            if( $ip == "::1"){
		                $ip = "127.0.0.1";
		            }/// end of default ip.........


	            //checking empty fileds
	            if($l == "" || $f == "" || $n == "" || $e == "" || $p == "" $dob == "" || $c == "" || $s == "" ){
	                 echo " <div class='alert alert-danger alert-dismissible' role='alert'>
	                                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
	                                        The form submission is missing values.
	                                    </div>";
	                exit();
	            } else if ($e_check > 0){ 
	                echo " <div class='alert alert-danger alert-dismissible' role='alert'>
	                                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
	                                        That email address is already in use in the system
	                                    </div>";
	                exit();
	            } else if (strlen($n) < 9 || strlen($n) > 10) {
	                 echo " <div class='alert alert-danger alert-dismissible' role='alert'>
	                                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
	                                        Phonenumber must be of Ghana only (+233) 240000001
	                                    </div>";
	                exit(); 
	            } else {

	            	// function to generate random string for activation...
	            	function randStrGen($len){
	                    $result = "";
	                    $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	                    $charArray = str_split($chars);
	                    for($i = 0; $i < $len; $i++){
	                        $randItem = array_rand($charArray);
	                        $result .= "".$charArray[$randItem];
	                    }
	                    return $result;
	                }//end of function for random string for activation...
	                //...........calling number of string to pass in function.........//
	                 $randstr = randStrGen(3);
	                //...............................................................//

	                 //lookup for last id in database for incremental of userid....

	                  $sqls = "SELECT id FROM Client WHERE id =(SELECT max(id) FROM Client) ";
	                    $quer = mysqli_query($cn, $sqls); 
	                    $e_check = mysqli_num_rows($quer);
	                    if($e_check > 0){
	                        while($rowzt = mysqli_fetch_array($quer, MYSQLI_ASSOC)){
	                            $lastnumber = $rowzt["id"];
	                        }
	                    }
	                //.................end of last id ....................//

	                //.........Setting number for id............//
	                    $setnum = 199;
		                $userkeynum = $setnum + $lastnumber;
		                $userkey = $randstr."-".$userkeynum
		            //.........end of id generation..........//
		                $p_hash = md5($p);
		                $uniq = "";
		                $uniq = uniqid();
		            //.............md5 hash password.........//
		            //...........unicq hex generation........//

		             // Add user info into the database table for the main site table
	                $sqlc = "INSERT INTO Client (firstname, lastname, email, dob, sex, Phonenumber, password, usernumber, ipdetect, notescheck )       
	                  VALUES('$f','$l','$e','$dob','$s','$n','$p','$userkey','$ip',now())";
	                $query = mysqli_query($cn, $sqlc); 
	                $uid = mysqli_insert_id($cn);

	        		// Adding user Id to Account table 
	                $sqlk = "INSERT INTO Account_info (usernumber) VALUES ('$userkey')";
	                $query = mysqli_query($cn, $sqlk);

	                // Establish their row in the useroptions table
	                $sql = "INSERT INTO Client_options (usernumber, email, background) VALUES ('$userkey','$e','original')";
	                $query = mysqli_query($cn, $sql);

	                 // Create directory(folder) to hold each user's files(pics, MP3s, etc.)
	                if (!file_exists("user/$userkey")) {
	                    mkdir("user/$userkey", 0755);
	                }

	                //seting direct files 
	              $dirName = dirname(__FILE__).DS.'community'.DS.'db'.DS.'users'.DS.$userkey;

	                mkdir($dirName);

	                $dataFiles = array(
	                    'logs' => 'logs.json',
	                    'payment' => 'payment.json',
	                    'notification' => 'notification.json',
	                    'messages' => 'messages.json'
	                    
	                );

	                foreach ($dataFiles as $key => $value) {

	                    $createFile = fopen($dirName. DS .$dataFiles[$key], 'w') or die('File not crreated');

	                    fclose($createFile);

	                }

	                mkdir($dirName.DS."media-resources");

	                mkdir($dirName.DS."feeds-comment-box");

	                mkdir($dirName.DS."conversations");
	                //end directory files setup

	                 // Email the user their activation link
	                $to = "$e";                          
	                $from = "Aegis.support@aegisghana.com";
	                $subject = 'Aegis Account Activation';
	                $message = '<!DOCTYPE html>
	                        <html>
	                        <head>
	                        <meta charset="UTF-8">
	                        <title>Aegis support Message</title>
	                        </head>
	                        <body class="with-you" style="background-color:#c6d1da; padding: 3%;">
	                                    <div class="Aegis" style="margin:0px; font-family:Tahoma, Geneva, sans-serif; background-color: #fff;">
	                                    <img src="http://localhost/Ag/1/App/img/Top Design.png" style="width: 49%;"/>
	                                    <center><img src="http://localhost/Ag/1/App/img/icons/logos/LOGO-DARK-MEDIUM.png" style="width: 29%;"></center>
	                                    <br/><br/>

	                                    <div class="Aegis" style="padding:7%; font-size:14px; padding-top: 0px; color: #666869; line-height: 29px;"><strong><h3 style="font-size: 21px; color: black">Hello '.$f.'</h3>
	                                    </strong> 
	                                    Welcome to the Aegis Device Care! We are super excited you have just taken a great step in experiencing a wholenew level of device care.<br />
	                                    <a style="color:#006faf;">Activate your account</a> to complete the registeration process and get full access to your account.Login after successful activation using your:<br />
	                                    <br />
	                                    <br />
	                                    <center> 
	                                        <button class="btn palette-Blue bg waves-effect" id="submit1" type="" href="http://www.aegisghana.com/overstreet/activation.php?id="'.$n.'&u='.$n.'&e='.$e.'&p='.$p.'" name="submit1" style="padding: 3.0%;padding-left: 133px;padding-right: 133px;border-radius: 50px; background-color: #267bbf; color: #fff; box-shadow: 0 2px 5px rgba(0, 0, 0, 0.16), 0 2px 10px rgba(0, 0, 0, 0.12); border: 0; position: relatcursor: pointer;display: inline-block; overflow: hidden; margin-bottom: 0; font-weight: 400; text-align: center; vertical-align: middle; touch-action: manipulation; white-space: nowrap;font-size: 17px;
	                                            line-height: 1.42857143; -webkit-font-smoothing: antialiased; font-family: inherit; -webkit-appearance: button; text-transform: none; background-image: none; width: 79%;">Activate Account</button>
	                                    </center>
	                                    </div>
	                            
	                                    <center><hr style="color: aliceblue;width: 80%;"></center>
	                                    <br/>
	                                    <br/>
	                                        <center>
	                                            <a style="color:#58b379;font-size: 100%;">We are working 24/7 together to give you the best service possible.</a>
	                                            </center>
	                                        <center>
	                                            <br/>
	                                            <center><a style="color:#8a949b; font-size: 79%;">Never hesitate to contact us when encounter a problem</a> </center>
	                                            <a style="color:#8a949b; font-size: 79%;"> -The Aegis Team</a>          
	                                        </center>
	                                    <br/>
	                                    <br/>
	                                    <footer style="color:#006faf; background-color:#f0f1f2;padding: 4%;">
	                                        <center>
	                                        <a><img src="http://localhost/Ag/1/App/img/logos/icons/small/facebook.png" style="width: 5%;" >
	                                            <img src="http://localhost/Ag/1/App/img/logos/icons/small/twitter.png" style="width: 5%;">
	                                            <img src="http://localhost/Ag/1/App/img/logos/icons/small/linkedin.png" style="width: 5%;">
	                                            <img src="http://localhost/Ag/1/App/img/logos/icons/small/youtube.png" style="width: 5%;">
	                                            </a>
	                                        </center>
	                                    </footer>
	                                </div>
	                           </body>
	                        </html>';
	                $headers = "From: $from\n";
	                $headers .= "MIME-Version: 1.0\n";
	                $headers .= "Content-type: text/html; charset=iso-8859-1\n";
	                mail($to, $subject, $message, $headers);
	                echo "signup_success";
	                exit();

	           
	            }//end last else of empty fileds
        //.................end of web code to run...............//

    			}else{
    				echo " <div class='alert alert-danger alert-dismissible' role='alert'>
                                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                                        email address entered in the system is not valid.
                                    </div>";
    			}
    		
    	}elseif( $c == "phone"){

    		//................phone code to run.........................//
            // DUPLICATE DATA CHECKS FOR USERNAME AND EMAIL
            $sql = "SELECT id FROM Client WHERE email ='$e' LIMIT 1";
            $query = mysqli_query($cn, $sql); 
            $e_check = mysqli_num_rows($query);

            else if ($e_check > 0){ 
	                echo "email error";
	                exit();
	        }else{
	            	///running up code for phone 
                    $sqls = "SELECT id FROM Client WHERE id =(SELECT max(id) FROM Client) ";
                    $quer = mysqli_query($cn, $sqls); 
                    $e_check = mysqli_num_rows($quer);
                    if($e_check > 0){
                        while($rowzt = mysqli_fetch_array($quer, MYSQLI_ASSOC)){
                            $lastnumber = $rowzt["id"];
                        }
                    }

                    function randStrGent($len){
                        $result = "";
                        $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                        $charArray = str_split($chars);
                        for($i = 0; $i < $len; $i++){
                            $randItem = array_rand($charArray);
                            $result .= "".$charArray[$randItem];
                        }
                        return $result;
                     }
                    $randst = randStrGent(3);


	                $setnum = 199;
	                $userkeynum = $setnum + $lastnumber;
	                $userkey = $randst."-".$userkeynum

	                $p_hash = md5($p);
	                $uniq = "";
	                $uniq = uniqid();

	            // Add user info into the database table for the main site table (without ip)
                $sqlc = "INSERT INTO Client (firstname, lastname, email, dob, sex, Phonenumber, password, usernumber, notescheck )       
                  VALUES('$f','$l','$e','$dob','$s','$n','$p','$userkey',now())";
                $query = mysqli_query($cn, $sqlc); 
                $uid = mysqli_insert_id($cn);

        
                $sqlk = "INSERT INTO Account_info (usernumber) VALUES ('$userkey')";
                $query = mysqli_query($cn, $sqlk);

                // Establish their row in the useroptions table
                $sql = "INSERT INTO Client_options (usernumber, email, background) VALUES ('$userkey','$e','original')";
                $query = mysqli_query($cn, $sql);

                // Create directory(folder) to hold each user's files(pics, MP3s, etc.)
                if (!file_exists("user/$userkey")) {
                    mkdir("user/$userkey", 0755);
                }


              $dirName = dirname(__FILE__).DS.'community'.DS.'db'.DS.'users'.DS.$userkey;
                mkdir($dirName);
                $dataFiles = array(

                    'logs' => 'logs.json',
                    'payment' => 'payment.json',
                    'notification' => 'notification.json',
                    'messages' => 'messages.json' 
                );
                foreach ($dataFiles as $key => $value) {

                    $createFile = fopen($dirName. DS .$dataFiles[$key], 'w') or die('File not crreated');

                    fclose($createFile);
                }

                mkdir($dirName.DS."media-resources");

                mkdir($dirName.DS."feeds-comment-box");

                mkdir($dirName.DS."conversations");

                	function randStrGen($len){
                        $result = "";
                        $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                        $charArray = str_split($chars);
                        for($i = 0; $i < $len; $i++){
                            $randItem = array_rand($charArray);
                            $result .= "".$charArray[$randItem];
                        }
                        return $result;
                     }
                    $randstr = randStrGen(6);

    

                    //Save the generated code in the database..
                    $sqi = "UPDATE Client_options SET twoway_code = $randstr WHERE email = '$e' LIMIT 1";
                    $agise = mysqli_query($cn, $sqi);

                    //Fined number of user....    
                    $sql = "SELECT * FROM Client  WHERE email = '$accnumber' AND password = '$p_hash' LIMIT 1";
                    //echo $sql;
                    $query = mysqli_query($cn,$sql) or die(mysqli_error());
                    $u_check = mysqli_num_rows($query);
                     while ($u_check = mysqli_fetch_array($query, MYSQLI_ASSOC)){
                         $idmail = $u_check["id"];
                         $number = $u_check["phonenumber"];
                     }



                     // Here we assume the user is using the combination of his clientId and clientSecret as credentials
                    $auth = new BasicAuth("oxndzfbu", "wighgefo");

                    // instance of ApiHost
                    $apiHost = new ApiHost($auth);
                    $enableConsoleLog = TRUE;
                    $messagingApi = new MessagingApi($apiHost, $enableConsoleLog);
                    // start try and catch..........
                    try {
                        // Quick Send approach options. Choose the one that meets your requirement

                        // Default Approach
                        $mesg = new Message();
                        $mesg->setContent("Enter the activation code to verify your account '".$randstr."'");
                        $mesg->setTo("+233'".$number."'");
                        $mesg->setFrom("Aegis Ghana");
                        $mesg->setRegisteredDelivery(true);
                    
                        $messageResponse = $messagingApi->sendMessage($mesg);
                    
                        if ($messageResponse instanceof MessageResponse) {
                           // echo $messageResponse->getStatus();
                        } elseif ($messageResponse instanceof HttpResponse) {
                           // echo "\nServer Response Status : " . $messageResponse->getStatus();
                        }
                    } catch (Exception $ex) {
                       // echo $ex->getTraceAsString();
                    }
                    //end try and catch..............

                    echo "success";
	        }//end of else of email check........


    	}//End of phone if statement..............
    	


}//isset post ending 