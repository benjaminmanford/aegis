<?php include 'classes/Adminlogin.php';?>
<?php
    $al = new Adminlogin();
    if ($_SERVER['REQUEST_METHOD'] == 'POST'){
      $adminUser = $_POST['adminUser'];
      $adminPass = md5($_POST['adminPass']);

      $loginChk = $al->adminLogin($adminUser,$adminPass);
    }
?>

<!DOCTYPE html>
 
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Aegis | Login</title>

        <script src='https://www.google.com/recaptcha/api.js'></script>
        
        <!-- Vendor CSS -->
        <link href="App/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="App/vendors/bower_components/google-material-color/dist/palette.css" rel="stylesheet">
        <link href="App/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
            
        <!-- CSS -->
        <link href="App/css/app.min.1.css" rel="stylesheet">
        <link href="App/css/app.min.2.css" rel="stylesheet">
        <script src ="App/js/main.js"></script>
        <script src ="App/js/ajax.js"></script>
        
    </head>
    
      <script>
       function emptyElement(x){
            _(x).innerHTML = "";
        }

            function restrict(elem){
                var tf = _(elem);
                var rx = new RegExp;
                 if(elem == "code"){
                    rx = /[^a-z-0-9]/gi;
                }else ifelem == "email"){
                    rx = /[]/gi;
                }
                tf.value = tf.value.replace(rx, "");
            }
    </script>
    <body>
        <div class="login" data-lbg="teal" style="background-color: white;">
            <!-- Login -->
            <div class="l-block toggled" id="l-login">
                <div class="lb-header palette-Teal bg" style="background-color: white;padding-bottom: 10px;">
                 <?php if (isset($Aultentication)){ ?>
                 <center><img src="App/img/icons/logos/LOGO-DARK-SMALL.png" alt="Agises " style="padding-bottom: 15px;">
                  <h3 style="font-size: 20px;">Enter Code</h3>
                   </center>
                </div>
<form class="sigin" action="" name="autentication_form" method="POST">
    <div class="lb-body" style="padding: 20px;">
            <div class="form-group fg-float">
                <div class="fg-line">
                <input class="input-sm form-control fg-input" onfocus="emptyElement('status')" onkeyup="restrict('code')" id="code" name="code" maxlength="8" required type="text"  require>
                <label class="fg-label">Activation Code <?php echo $Aultentication; ?></label>
                                        </div>
                                    </div>

     <button class='btn palette-Blue bg waves-effect' id='submit' type='submit' name='submit' style='padding: 14px;padding-left: 133px;padding-right: 133px;border-radius: 50px; background-color: #267bbf;'>LOG IN</button>

                                </div>
                            </form>
                      <?php  }else{?>
         <center><img src="App/img/icons/logos/LOGO-DARK-SMALL.png" alt="Agises " style="padding-bottom: 15px;">

                   <?php
                    if(isset($Agiesmsg)){
                       echo $Agiesmsg;
                    } else{ ?>
                       <h3 style="font-size: 20px;">Log into your account</h3>
                    <?php } ?>
                   </center>
                </div>
           <form class="sigin" action="" name="login_form" method="POST" id='i-recaptcha'>
                <div class="lb-body" style="padding: 20px;">
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            <input class="input-sm form-control fg-input" onkeyup="restrict('email')" id="email" name="accnumber" maxlength="38" required type="text"  require>
                            <label class="fg-label">Email</label>
                        </div>
                    </div>

                    <div class="form-group fg-float">
                        <div class="fg-line">
                            <input class="input-sm form-control fg-input" id="password" name="password" required type="password" require >
                            <label class="fg-label">Password</label>
                        </div>
                    </div>
             
            <button class="btn palette-Blue bg waves-effect " type="submit" name = "submit" style="padding: 14px;padding-left: 133px;padding-right: 133px;border-radius: 50px; background-color: #267bbf; box-shadow: 0 2px 5px rgba(0, 0, 0, 0.16), 0 2px 10px rgba(0, 0, 0, 0.12); color: #fff; border-color: #267bbf;">LOGIN</button>
            <br/><br/>
           <!-- <div class="g-recaptcha" data-sitekey="6LeTEDIUAAAAACx0I9-LD8yxboQdC0Yxg0NkKYfH"></div>-->

                    <div class="m-t-20">
                        
                        <a  data-bg="purple" href="forgot.php" class="pcol-sm-3" style="color: black;" >Forgot password?</a>

                        <a data-bg="blue" class="pcol-sm-3" href="register.php" style="float: right;">Creat an account</a>
                    </div>
                </div>
            </form>
            <?php } ?>
            </div>

            
        </div>

 

        <!-- Javascript Libraries -->
        <script src="App/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="App/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="App/vendors/bower_components/Waves/dist/waves.min.js"></script>
       
        <script src="App/js/functions.js"></script>
        
    </body>
</html>