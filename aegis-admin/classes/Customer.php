<?php 
    $filepath = realpath(dirname(__FILE__));
  include_once ($filepath.'/../lib/Database.php');
  include_once ($filepath.'/../helpers/Format.php');

class Customer{
	
	private $db;
	private $fm;

	public function __construct(){
      $this->db = new Database();
      $this->fm = new Format();
   }

    public function customerRegistration($data){
      $name       = mysqli_real_escape_string($this->db->link, $data['name']);
      $address    = mysqli_real_escape_string($this->db->link, $data['address']);
      $city       = mysqli_real_escape_string($this->db->link, $data['city']);
      $zip        = mysqli_real_escape_string($this->db->link, $data['zip']);
      $country    = mysqli_real_escape_string($this->db->link, $data['country']);
      $phone      = mysqli_real_escape_string($this->db->link, $data['phone']);
      $email      = mysqli_real_escape_string($this->db->link, $data['email']);
      $password   = mysqli_real_escape_string($this->db->link, md5($data['password']));

      if ($name == "" || $city == "" || $country == "" || $phone == "" || $email == "" || $password == ""){
        $msg = "<div class='alert alert-danger alert-dismissible' role='alert'>
                <strong><span class='text-capitalize'>Sorry </span></strong>Field must not be empty ! 
                </div>";
      return $msg;
    }

    $mailquery = "SELECT * FROM tbl_customer WHERE email = '$email' LIMIT 1";
    $mailchk = $this->db->select($mailquery);
    if ($mailchk != false) {
       $msg = "<div class='alert alert-danger alert-dismissible' role='alert'>
                <strong><span class='text-capitalize'>Sorry </span></strong>Email already Exist ! 
                </div>";
      return $msg;
    }else{

    $query = "INSERT INTO tbl_customer(name,address,city,zip,country,phone,email,pass) VALUES('$name','$address','$city','$zip','$country','$phone','$email','$password')";
     $inserted_row = $this->db->insert($query);
      if ($inserted_row){
        $msg = "<div class='alert alert-success alert-dismissible' role='alert'>
                <strong><span class='text-capitalize'></span></strong>Customer Registration Successful. 
                  </div>";
        return $msg;
      }else{
        $msg = "<div class='alert alert-danger alert-dismissible' role='alert'>
                <strong><span class='text-capitalize'>Sorry </span></strong>Customer Registration not Successful . 
                                        </div>";
        return $msg;
      }
     }
 }

    public function customerLogin($data){
      $email      = mysqli_real_escape_string($this->db->link, $data['email']);
      $password   = mysqli_real_escape_string($this->db->link, md5($data['password']));

        if ($email == "" || $password == ""){
        $msg = "<div class='alert alert-danger alert-dismissible' role='alert'>
                <strong><span class='text-capitalize'>Sorry </span></strong>Field must not be empty ! 
                </div>";
      return $msg;
    }   

    $query = "SELECT * FROM tbl_customer WHERE email = '$email' AND pass = '$password' LIMIT 1";
    $result = $this->db->select($query);
    if ($result != false) {
      $value = $result->fetch_assoc();
      Session::set("cusLogin", true);
      Session::set("cmrId",$value['id']);
      Session::set("cmrName",$value['name']);
       header("Location:order.php"); 
    }else{
            $msg = "<div class='alert alert-danger alert-dismissible' role='alert'>
                <strong><span class='text-capitalize'>Sorry </span></strong>Customer details does not match ! 
                </div>";
      return $msg;
    }

    }
}
?>