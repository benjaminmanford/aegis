<header id="header">
	<aside class="topbar" style="background-color: #113352; margin-top: 0px;">
		<div class="container">
			<ul class="touch">
				<li>
					<img src="demo/img/phone.png" alt="Phone" title="Phone">
					<p>+2332-4903-5899<br />+2332-0445-6345</p>
				</li>
				<li>
					<img src="demo/img/mail.png" alt="Mail" title="Mail">
					<p><a href="mailto:info@aegisghana.com">support@aegisghana.com</a><br /><a href="mailto:info@aegisghana.com">info@aegisghana.com</a></p>
				</li>
				<li>
					<img src="demo/img/map.png" alt="Map" title="Map">
					<p>124 Freetown Avenue,<br />East Legon, Accra</p>
				</li>
			</ul>
			<ul class="user-nav">
				<li><a href="instract/login" data-toggle="modal" title="Create an account" class="btn" style="border-radius: 50px; background-color: #007aff;padding: 7px 13px 7px 13px; margin-right: 7px;">PAY PREMIUM</a></li>
				<li><a href="#login-register" data-toggle="modal" title="Create an account" class="btn" style="border-radius: 50px;background-color: transparent;border-width: 2px;border-color: white; padding: 7px 13px 7px 13px; margin-right: 7px;">GET A QUOTE</a></li>
				<li><a href="#login-register" data-toggle="modal" title="Create an account" class="btn" style="border-radius: 50px;background-color: #e74c3c;border-color: #e74c3c; padding: 7px 13px 7px 13px;">FILE A CLAIM</a></li>
			</ul>
		</div>
	</aside>
	<div class="navbar megamenu-width">
		<div class="container">
			<aside id="main-search">
				<form action="#" method="GET">
					<a href="#" class="close">
						<i class="icomoon-close"></i>
					</a>
					<div class="form-field" style="">
						<div class="placeholder" style="">
							<label for="query">what do you need help with?</label>
							<input class="form-control" type="text" name="query" id="query" style="background: #d6dee1; border-color: white;color: #000"/>
						</div>
					</div>
				</form>
			</aside>
			<div class="navbar-inner">
				<a href="Home" class="logo"  style="height: 46px;">
					<img src="App/img/icons/logos/LOGO-DARK-SMALL.png" alt="Aegis_logo " style="max-height: 128%;max-width: 107%;margin-top: -3%;">
				</a>
				<ul id="mobile-menu">
					<li>
						<a href="login" data-toggle="modal" title="Log in">Log In</a>
					</li>
					<li>
						<a href="Register" data-toggle="modal" title="Create an account">Register</a>
					</li>
					<li>
						<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
							<i class="fa fa-reorder"></i>
						</button>
					</li>
					<li>
						<a class="btn-search" href="#"><i class="fa fa-search"></i></a>
					</li>
				</ul>
				<ul id="main-menu" class="collapse navbar-collapse nav slide">
					<li class='active'>
						<a href="index.php">Home</a>
					</li>
					<li class="megamenu">
						<a href="convenience.php">OUR DIFFERENCE</a>
					</li>
				<li>
					<a href="#">
						PLANS<i class="carret"></i>
					</a>
					<ul class="dropdown">
						<li><a href="single-device">SINGLE DEVICE</a></li>
						<li><a href="student">STUDENT</a></li>
						<li><a href="multidevice">MULTI-DEVICE</a></li>
						<li><a href="shortcodes-alerts-messages.html">CORPORATE</a></li>
						<li><a href="family">FAMILY</a></li>
						<li><a href="features-animation.html">INSTITUTION</a></li>
					</ul>
				</li>
					<li style="padding-bottom: 30px; padding-right: 10px;">
						<a href="#">
							PROTECTION<i class="carret"></i>
						</a>
						<ul class="dropdown">
							<li><a href="phone">PHONE</a></li>
							<li><a href="tablet">TABLET</a></li>
							<li><a href="laptop">LAPTOP</a></li>
						</ul>
					</li>
					<li style="padding-bottom: 30px; ">
						<a href="#" style="padding-right: 25px;">
							ABOUT<i class="carret"></i>
						</a>
						<ul class="dropdown">
							<li><a href="about">ABOUT US</a></li>
							<li><a href="contact">CONTACT US</a></li>
							<li><a href="team">OUR TEAM</a></li>
							<li><a href="faqs">FAQS</a></li>
						</ul>
					</li>
					<li style="padding-bottom: 30px;">
						<a href="login.php" style="border: 1px solid #2e343f; margin-right: 12px; ">
							LOG IN / REGISTER
						</a>
						<ul class="dropdown">
							<li><a href="login">LOGIN</a></li>
							<li><a href="register">REGISTER</a></li>
						</ul>
					</li>
					<li class="search-nav">
						<a href="#" class="btn-search"><i class="fa fa-search"></i></a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</header>