<footer id="footer" style="background-color: #123354;">
		<div class="container">
			<div class="row row-inline">
				<div class="col-sm-6">

					<div class="title title-main">
						<h5>Newsletter</h5>
					</div>
					<div class="text">
						<p>Keep up on our always evolving product features and technology. Enter your e-mail and subscribe to our newsletter.</p>
					</div>

				</div><!-- .col-sm-6 -->
				<div class="col-sm-6">

					<form method="POST" id="my_Newsletter" onsubmit="return false;" autocomplete="on" >

						<button id="newsletter" type="submit" onclick="Newsletter()" class="btn btn-big" style="float: right;float: inline-end;background-color: white; border-color: #123354; color: #123354;">SEND</button>

						<div class="form-field" style="width: 77%;">
							<div class="placeholder">
								<label for="subscribeemail">Please enter your email</label>
								<input class="form-control" type="email" maxlength="100" name="subscribeemail" id="subscribeemail" required />
							</div>
						</div>
					</form>

				</div><!-- .col-sm-6 -->
			</div><!-- .row-->

			<hr class="devider-heavy" />
			<div class="row">
				<div class="col-sm-4">

					<div class="widget">
						<div class="widget-heading">

							<div class="title title-main">
								<a href="index.html">
									<img src="App/img/LOGO/WITH SLOGAN/LOGO-LIGHT-SMALL.png" alt="Aegisghana">
								</a>
							</div>

						</div><!-- .widget-heading -->
						<div class="widget-content">

							<div class="text">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur augue nunc, tempus id imperdiet eu.</p>
								<p>Mauris pulvinar est in quam dapibus a bibendum. Lorem ipsum dolor sit amet, consectetur Lorem ipsum dolor. </p>
							</div>

						</div><!-- .widget-content -->
					</div><!-- .widget -->

				</div><!-- .col-sm-3 -->
				<div class="col-sm-2">

					<div class="widget">
						<div class="widget-heading">

							<div class="title title-main">
								<h5>PLANS</h5>
							</div>

						</div><!-- .widget-heading -->
						<div class="widget-content">

							<section class="posts">
								<article class="post post-mini post-type-text">
								<ul class="nav">
								  <a href="index.html">STANDARD</a><br/>
								  <a href="#">STUDENT</a><br/>
								  <a href="#">CORPORATE</a><br/>
								  <a href="#">FAMILY</a><br/>
								  <a href="#">INSTITUTION</a><br/>
								  <a href="#">MULTI-DEVICE</a><br/>
								</ul>
									
								</article><!-- .post -->
							</section><!-- .posts -->

						</div><!-- .widget-content -->
					</div><!-- .widget -->

				</div><!-- .col-sm-2 -->




                <div class="col-sm-2">

					<div class="widget">
						<div class="widget-heading">

							<div class="title title-main">
								<h5>PROTECTION</h5>
							</div>

						</div><!-- .widget-heading -->
						<div class="widget-content">

							<section class="posts">
								<article class="post post-mini post-type-text">
								<ul class="nav">
								  <a href="index.html">MOBILE PHONE</a><br/>
								  <a href="#">TABLET</a><br/>
								  <a href="#">LAPTOP</a><br/>
								  <!--<a href="#">CAMERA</a><br/>-->
								</ul>
									
								</article><!-- .post -->
							</section><!-- .posts -->

						</div><!-- .widget-content -->
					</div><!-- .widget -->

				</div><!-- .col-sm-2 -->


				<div class="col-sm-2">

					<div class="widget">
						<div class="widget-heading">

							<div class="title title-main">
								<h5>ABOUT</h5>
							</div>

						</div><!-- .widget-heading -->
						<div class="widget-content">

							<section class="posts">
								<article class="post post-mini post-type-text">
								<ul class="nav">
								  <a href="index.html">OUR TEAM</a><br/>
								  <a href="#">CONTACT US</a><br/>
								  <a href="#">ABOUT US</a><br/>
								  <a href="#">OUR DIFFERENCE</a><br/>
								  <a href="#">FAQ</a><br/>
								  <!--<a href="#">BLOG</a><br/>-->
								</ul>
									
								</article><!-- .post -->
							</section><!-- .posts -->

						</div><!-- .widget-content -->
					</div><!-- .widget -->

				</div><!-- .col-sm-2 -->


				<div class="col-sm-2">

					<div class="widget">
						<div class="widget-heading">

							<div class="title title-main">
								<h5>QUICK LINKS</h5>
							</div>

						</div><!-- .widget-heading -->
						<div class="widget-content">

							<section class="posts">
								<article class="post post-mini post-type-text">
								<ul class="nav">
								  <a href="index.html">LOG IN</a><br/>
								  <a href="#">CREATE AN ACCOUNT</a><br/>
								  <a href="#">FILE A CLAIM</a><br/>
								  <a href="#">FAMILY</a><br/>
								  <a href="#">GET A QUOTE</a><br/>
								</ul>
									
								</article><!-- .post -->
							</section><!-- .posts -->

						</div><!-- .widget-content -->
					</div><!-- .widget -->

				</div><!-- .col-sm-2 -->

				
				
			</div><!-- .row-->
			<hr class="devider-heavy" />
			<ul class="nav text-center">
				<li><a href="index.html">Home</a></li>
				<li><a href="#">REGISTER</a></li>
				<li><a href="#">LOG IN</a></li>
				<li><a href="#">FILE A CLAIM</a></li>
				<li><a href="#">PAY PREMIUM</a></li>
				<li><a href="#">GET A QUOTE</a></li>
				<li><a href="#">CONTACT US</a></li>
				
			</ul>
			<hr class="devider-heavy" />
			<div class="widget">
						<div class="widget-heading">

							<div class="title title-main">
								<h5>WE ACCEPT THE FOLLOWING FORMS OF PAYMENT</h5>
							</div>

						</div><!-- .widget-heading -->
			<ul class="nav">
				<a><img src="demo/img/elements/visa.png" alt="visa" style="height: 15%; width: 11.95%;"></a>
				<a><img src="demo/img/elements/mastercard.png" alt="mastercard" style="height: 15%; width: 12.06%;"></a>
				<a><img src="demo/img/elements/tigo-cash.png" alt="tigo" style="height: 15%; width: 7.33%;"></a>
				<a><img src="demo/img/elements/airtel-money.png" alt="airtel" style="height: 15%; width: 9.64%;"></a>
				<a><img src="demo/img/elements/vodafone-cash.png" alt="vodafone" style="height: 15%; width: 12.75%;"></a>
				<a><img src="demo/img/elements/mtn-mobile-money.png" alt="mtn" style="height: 15%; width: 8.56%;"></a>
			</ul>
			<hr class="devider-heavy" />
			<div class="row-inline-wrap">
				<div class="row row-inline">
					<div class="col-md-7">

						<ul class="touch">
							<li><i class="fa icomoon-location"></i><p>124 Freetown Avenue,<br />East Legon, Accra</p></li>
							<li><i class="fa fa-phone"></i><p>+2332-4903-5899<br />+2332-0445-6345</p></li>
							<li><i class="fa fa-envelope"></i><p><a href="mailto:supprot@aegisghana.com">supprot@aegisghana.com</a><br /><a href="mailto:info@aegisghana.com">info@aegisghana.com</a></p></li>
						</ul>

					</div><!-- .col-md-7 -->
					<div class="col-md-5">

						<ul class="social">
							<!--<li><a href="#" class="rss"></a></li>-->
							<li><a href="#" class="google"></a></li>
							<!--<li><a href="#" class="vimeo"></a></li>-->
							<li><a href="#" class="youtube"></a></li>
							<li><a href="#" class="facebook"></a></li>
							<li><a href="#" class="twitter"></a></li>
						</ul>

					</div><!-- .col-md-5 -->
				</div><!-- .row -->
			</div><!-- .row-inline-wrap -->
		</div><!-- .container -->
		<div class="credits" style="background-color: #112c47">Aegisghana © 2017<span>|</span><a href="#">Terms</a><span>|</span><a href="#">Legal Notice</a></div>
	</footer>


	<a class="btn btn-icon-top" id="toTop" href="#"></a>

	<!-- jQuery & Helper library -->
	<script type="text/javascript" src="demo/js/library/jquery/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="demo/js/library/jquery/jquery-ui-1.10.4.custom.min.js"></script>

	<!-- Retina js -->
	<script type="text/javascript" src="demo/js/library/retina/retina.min.js"></script>

	<!-- FancyBox -->
	<script type="text/javascript" src="demo/js/library/fancybox/jquery.fancybox.pack8cbb.js?v=2.1.5"></script>

	<!-- Bootstrap js -->
	<script type="text/javascript" src="demo/js/library/bootstrap/bootstrap.min.js"></script>

	<!-- Validate -->
	<script type="text/javascript" src="demo/js/library/validate/jquery.validate.min.js"></script>

	<!-- FlickrFeed  -->
	<script type="text/javascript" src="demo/js/library/jFlickrFeed/jflickrfeed.min.js"></script>
	
	<!-- carouFredSel -->
	<script type="text/javascript" src="demo/js/library/carouFredSel/jquery.carouFredSel-6.2.1-packed.js"></script>
					
	<!-- Mediaelementjs -->
	<script type="text/javascript" src="demo/js/library/mediaelementjs/mediaelement-and-player.min.js"></script>
		
	<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
	<script type="text/javascript" src="demo/js/library/slider-revolution/js/jquery.themepunch.tools.min.js"></script>
	<script type="text/javascript" src="demo/js/library/slider-revolution/js/jquery.themepunch.revolution.min.js"></script>
	<script type="text/javascript" src="demo/js/slider.js"></script>
	
	<!-- Main theme javaScript file -->
	<script type="text/javascript" src="demo/js/theme.js"></script>
	
	<!-- Options -->
	<script type="text/javascript" src="../CORE/js/library/jquery.cookie.js"></script>
	<script type="text/javascript" src="../CORE/js/options.js"></script>
	<script type="text/javascript">
		function Newsletter(){
            var u = _("subscribeemail").value;
            if(u != ""){
                _("my_Newsletter").innerHTML = '<div class="call2action call2action-colored stripes animated"><div class="call2action-heading"><div class="text"><p> Validating Email... </p></div></div></div>';
                var ajax = ajaxObj("POST", "function/postnews.php");
                ajax.onreadystatechange = function() {
                    if(ajaxReturn(ajax) == "success") {
                        _('my_Newsletter').innerHTML = '<div class="call2action call2action-colored stripes animated" style="border-color: #3bb3e0;background-color: #47b2e5;"><div class="call2action-heading"><div class="text"><p> '+_("subscribeemail").value+ 'is set for Newsletters </p></div></div></div>';
                    }else{
                    	_("my_Newsletter").innerHTML = ajax.responseText;
                    }
                }
                ajax.send("usernamecheck="+u);
            }
        }
	</script>
	</body>

<!-- Mirrored from jets.funcoders.com/demo/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 17 Oct 2014 22:17:04 GMT -->
</html>