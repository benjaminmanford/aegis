<!DOCTYPE html>
	<html> <!--<![endif]-->
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<head>
		<!-- Basic -->
		<meta charset="utf-8">
		<title><?php echo $pageTitle; ?>  - Aegis</title>

		<meta name="keywords" content="aegisghana" />
		<meta name="description" content="AEGIS - Home">
		<meta name="author" content="aegisghana.com">

		<!-- Favicons -->
		<link rel="shortcut icon" type='image/x-icon' href="demo/img/favicon/favicon.ico">
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="demo/img/favicon/fapple-touch-icon-144x144-precomposed.html">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="demo/img/favicon/fapple-touch-icon-114x114-precomposed.html">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="demo/img/favicon/fapple-touch-icon-72x72-precomposed.html">
		<link rel="apple-touch-icon-precomposed" href="demo/img/favicon/fapple-touch-icon-precomposed.html">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="demo/css/library/bootstrap/bootstrap.min.css" />

		<!-- AWESOME and ICOMOON fonts -->
		<link rel="stylesheet" href="demo/css/fonts/awesome/css/font-awesome.css">
		<link rel="stylesheet" href="demo/css/fonts/icomoon/style.css">

		<!-- Open Sans fonts -->
		<link rel="stylesheet"  href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800">

		<!-- Theme CSS -->
		<!-- <link rel="stylesheet" href="css/theme.min.css"> -->
		<link rel="stylesheet" href="demo/css/theme.css">
		<link rel="stylesheet" href="demo/css/hover.css" media="all">
		<link rel="stylesheet" href="demo/css/theme-elements.min.css">
		<link href="App/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
		<!-- Slider revolution -->
		<link rel="stylesheet" href="demo/js/library/slider-revolution/css/settings.css">
		<link rel="stylesheet" href="demo/css/color/blue.css" id="layout-color">

		<!-- Theme Options -->
		<link rel="stylesheet" href="CORE/css/options.css">

		<link rel="stylesheet" href="demo/css/library/animate/animate.min.css">

		<!-- Your styles -->
		<link rel="stylesheet" href="demo/css/styles.css">

	    <script type="text/javascript">
			function Newsletter(){
	            var u = _("email").value;
	            if(u != ""){
	                _("my_Newsletter").innerHTML = '<div class="call2action call2action-colored stripes animated"><div class="call2action-heading"><div class="text"><p> Validating Email... </p></div></div></div>';
	                var ajax = ajaxObj("POST", "function/postnews.php");
	                ajax.onreadystatechange = function() {
	                    if(ajaxReturn(ajax) == "success") {
	                        _('my_Newsletter').innerHTML = '<div class="call2action call2action-colored stripes animated" style="border-color: #3bb3e0;background-color: #47b2e5;"><div class="call2action-heading"><div class="text"><p> V' . $email . 'is set for Newsletters </p></div></div></div>';
	                    }else{
	                    	_("my_Newsletter").innerHTML = ajax.responseText;
	                    }
	                }
	                ajax.send("usernamecheck="+u);
	            }
	        }
		</script>
	</head>