<?php
if(isset($_SESSION["AGXusername"])){
    echo'<script>window.location="App/dexoption.php";</script>';
    exit();
}
 //require 'function/log.php';

$message = "";
$Errorm = "";
$haerd = "";
$tt = "";

$msg = preg_replace('#[^a-z 0-9.:_()]#i', '', $_GET['msg']);

if ($msg != ""){
    
    ////////////if message is in the ms box/////////////////
    if($msg == "activation_failure"){
    $haerd = "<h2 style='color: #ffffff;  margin-top: -6px;'>Activation Error<h2>";
    $message = " Sorry there seems to have been an issue activating your account at this time. We have already notified ourselves of this issue and we will contact you via email when we have identified the issue.";
      $Errorm = "<h2 style='color: #e25500;  font-size: 40px; margin-bottom: 30px; font-family: Open Sans;'>Activation Error</h2>";
    } else if($msg == "activation_success"){
       $haerd ="Activation Success";
        $message = "Your account is now activated. <a href='login'>Click here to log in</a>";
      $Errorm = "<h2 style='color: #69fe9d;  font-size: 40px; margin-bottom: 30px; font-family: Open Sans;'>Activation Successful</h2>";
    } else if($msg == "Activation_sent"){
       $haerd ="<h3>Please check your email</h3><br/><img src='App/img/mail.png' alt='Agises' style='padding-bottom: 15px;width: 30%;'>";
        $message = "<br/><br/><a style='color: black; font-size: 126%;'>We've just sent you an email to activate your account. it should be there in an instant but can take up to five mainutes to arrive</a><br/><br/><br/><br/>
    <button class='btn palette-Blue bg waves-effect' id='submit1' type='submit' name='submit1' style='padding: 14px;padding-left: 133px;padding-right: 133px;border-radius: 50px; background-color: #267bbf;'>LOG IN</button>";
    }else {
      $Errorm = "<h2 style='color: #e25500;  font-size: 40px; margin-bottom: 30px; font-family: Open Sans;'>Activation Error</h2>";
        $message = $msg;
    }
    ///////////end of ms box messaging ...//////////////////

}else{

    //echo'<script>window.location="login.php";</script>';
    //$tt = "<><img src='App/img/mail.png' alt='Agises' style='padding-bottom: 15px;width: 41%;'>";
    $haerd ="<h3>Please check your email</h3><br/><img src='App/img/mail.png' alt='Agises' style='padding-bottom: 15px;width: 30%;'>";
    $message = $msg;

}


?>
<!DOCTYPE html>
 
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Aegis | message</title>
        
        <!-- Vendor CSS -->
        <link href="App/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="App/vendors/bower_components/google-material-color/dist/palette.css" rel="stylesheet">
        <link href="App/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
            
        <!-- CSS -->
        <link href="App/css/app.min.1.css" rel="stylesheet">
        <link href="App/css/app.min.2.css" rel="stylesheet">
        <script src ="App/js/main.js"></script>
        <script src ="App/js/ajax.js"></script>
        
    </head>
    
    <body>
        <div class="login" data-lbg="teal" style="background-color: white;">
            <!-- Login -->
            <div class="l-block toggled" id="l-login">
            <center><img src="App/img/icons/logos/LOGO-DARK-SMALL.png" alt="Agises " style="padding-bottom: 15px;width: 58%;">
                <center><?php echo $haerd;?></center>
                <center><?php echo $message;?></center>

            </div>

            
        </div>

 

        <!-- Javascript Libraries -->
        <script src="App/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="App/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="App/vendors/bower_components/Waves/dist/waves.min.js"></script>
       
        <script src="App/js/functions.js"></script>
        
    </body>
</html>