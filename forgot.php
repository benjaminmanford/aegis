<!DOCTYPE html>
 
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Aegis | Home</title>
        
        <!-- Vendor CSS -->
        <link href="App/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="App/vendors/bower_components/google-material-color/dist/palette.css" rel="stylesheet">
        <link href="App/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
            
        <!-- CSS -->
        <link href="App/css/app.min.1.css" rel="stylesheet">
        <link href="App/css/app.min.2.css" rel="stylesheet">
    </head>
    
    <body>
        <div class="login" data-lbg="teal" style="background-color: white;">
            <!-- Login -->
            <div class="l-block toggled" id="l-login">
                <div class="lb-header palette-Teal bg" style="background-color: white;padding-bottom: 10px;">
                   <center><img src="App/img/icons/logos/LOGO-DARK-SMALL.png" alt="Agises " style="padding-bottom: 15px;">
                                <h3 style="font-size: 20px;">RESET YOUR PASSWORD</h3>
                                <P style="color: #9c9999; font-size: smaller;">A link will be sent to your email to rest your password</P>
                   </center>
                </div>

                <div class="lb-body" style="padding: 20px;">
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            <input type="text" class="input-sm form-control fg-input">
                            <label class="fg-label">Email </label>
                        </div>
                    </div>


                   <button class="btn palette-Blue bg waves-effect" style="padding: 14px;padding-left: 133px;padding-right: 133px;border-radius: 50px; background-color: #1767a7;">SEND</button>

                    <div class="m-t-20">
                        
                        <a  data-bg="purple" href="login.php" class="pcol-sm-3" style="color: black;" >Got an account</a>

                        <a data-bg="blue" class="pcol-sm-3" href="register.php" style="float: right;">Create an account</a>
                    </div>
                </div>
            </div>

            
        </div>

        <!-- Older IE warning message -->
        <!--[if lt IE 9]>
            <div class="ie-warning">
                <h1 class="c-white">Warning!!</h1>
                <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
                <div class="iew-container">
                    <ul class="iew-download">
                        <li>
                            <a href="http://www.google.com/chrome/">
                                <img src="img/browsers/chrome.png" alt="">
                                <div>Chrome</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.mozilla.org/en-US/firefox/new/">
                                <img src="img/browsers/firefox.png" alt="">
                                <div>Firefox</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.opera.com">
                                <img src="img/browsers/opera.png" alt="">
                                <div>Opera</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.apple.com/safari/">
                                <img src="img/browsers/safari.png" alt="">
                                <div>Safari</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                                <img src="img/browsers/ie.png" alt="">
                                <div>IE (New)</div>
                            </a>
                        </li>
                    </ul>
                </div>
                <p>Sorry for the inconvenience!</p>
            </div>
        <![endif]-->

        <!-- Javascript Libraries -->
        <script src="App/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="App/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="App/vendors/bower_components/Waves/dist/waves.min.js"></script>

        <!-- Placeholder for IE9 -->
        <!--[if IE 9 ]>
            <script src="vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
        <![endif]-->

        <script src="App/js/functions.js"></script>
        
    </body>
</html>